﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="FAQs.aspx.cs" Inherits="macEgypt.FAQs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CPTitle" runat="server">
    Frequently Asked Questions
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPHead" runat="server">
    <style>
        .page-header {
            border-bottom: none;
            margin-top: 0px;
        }

        span.dropcap1 {
            height: 50px;
            width: 50px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CPContent" runat="server">
    <div id="main-content">
        <div id="sub-header" class="layout-full has-background">
            <div class="meta-header" style="background-color: #717171; background-image: url(https_/construction.vamtam.com/wp-content/uploads/2014/07/header_bg-3.html); background-repeat: no-repeat; background-attachment: fixed;">
                <div class="limit-wrapper">
                    <div class="meta-header-inside">
                        <header class="page-header ">
                            <div class="page-header-content">
                                <h1 style="color: #fff;">
                                    <span class="title">
                                        <span itemprop="headline">FAQ</span>
                                        <div class="desc" style="color: #fff;">JUST FIND YOUR ANSWERS BELOW</div>
                                    </span>
                                </h1>
                            </div>
                        </header>
                    </div>
                </div>
            </div>
        </div>
        <div id="main" role="main" class="wpv-main layout-full">
            <div class="row page-wrapper">
                <article id="post-8024" class="full post-8024 page type-page status-publish hentry">
                    <div class="page-content">
                        <div class="push  wpv-hide-lowres" style="height: 30px"></div>
                        <div class="limit-wrapper">
                            <div class="row ">
                                <div class="wpv-grid grid-1-1  wpv-first-level first unextended no-extended-padding" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-42b51464dae418dd6f1d6c6f631f7135">
                                    <div class="clearfix dropcap-wrapper">
                                        <div class="dropcap-left"><span class='dropcap1 '>Q</span></div>
                                        <div class="dropcap-text">We will fight to protect your legal rights under the law?</div>
                                    </div>
                                    <div class="push " style="height: 20px"></div>
                                    <span class='dropcap1 '>A</span>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.<br />
                                    Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet.
                                </div>
                            </div>
                        </div>
                        <div class="limit-wrapper">
                            <div class="sep-3"></div>
                        </div>
                        <div class="limit-wrapper">
                            <div class="row ">
                                <div class="wpv-grid grid-1-1  wpv-first-level first unextended no-extended-padding" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-a1c9cfd1a3367d8185f4da5ae0c85ca0">
                                    <div class="clearfix dropcap-wrapper">
                                        <div class="dropcap-left"><span class='dropcap1 '>Q</span></div>
                                        <div class="dropcap-text">We will fight to protect your legal rights under the law?</div>
                                    </div>
                                    <div class="clearboth"></div>
                                    <div class="push " style="height: 20px"></div>
                                    <span class='dropcap1 '>A</span>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.<br />
                                    Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet.
                                </div>
                            </div>
                        </div>
                        <div class="limit-wrapper">
                            <div class="sep-3"></div>
                        </div>
                        <div class="limit-wrapper">
                            <div class="row ">
                                <div class="wpv-grid grid-1-1  wpv-first-level first unextended no-extended-padding" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-17f0e750e8cba3ab6c06c397c96809b4">
                                    <div class="clearfix dropcap-wrapper">
                                        <div class="dropcap-left"><span class='dropcap1 '>Q</span></div>
                                        <div class="dropcap-text">We will fight to protect your legal rights under the law?</div>
                                    </div>
                                    <div class="clearboth"></div>
                                    <div class="push " style="height: 20px"></div>
                                    <span class='dropcap1 '>A</span>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.<br />
                                    Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet.
                                </div>
                            </div>
                        </div>
                        <div class="limit-wrapper">
                            <div class="sep-3"></div>
                        </div>
                        <div class="limit-wrapper">
                            <div class="row ">
                                <div class="wpv-grid grid-1-1  wpv-first-level first unextended no-extended-padding" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-225d21087b71aecf4a61c55b76c92efe">
                                    <div class="clearfix dropcap-wrapper">
                                        <div class="dropcap-left"><span class='dropcap1 '>Q</span></div>
                                        <div class="dropcap-text">We will fight to protect your legal rights under the law?</div>
                                    </div>
                                    <div class="clearboth"></div>
                                    <div class="push " style="height: 20px"></div>
                                    <span class='dropcap1 '>A</span>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.<br />
                                    Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet.
                                </div>
                            </div>
                        </div>
                        <div class="limit-wrapper">
                            <div class="sep-3"></div>
                        </div>
                        <div class="limit-wrapper">
                            <div class="row ">
                                <div class="wpv-grid grid-1-1  wpv-first-level first unextended no-extended-padding" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-46c625cf499d7b42319ac7148fe647b7">
                                    <div class="clearfix dropcap-wrapper">
                                        <div class="dropcap-left"><span class='dropcap1 '>Q</span></div>
                                        <div class="dropcap-text">We will fight to protect your legal rights under the law?</div>
                                    </div>
                                    <div class="clearboth"></div>
                                    <div class="push " style="height: 20px"></div>
                                    <span class='dropcap1 '>A</span>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.<br />
                                    Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet.
                                </div>
                            </div>
                        </div>
                        <div class="push " style="height: 60px"></div>
                        <div class="limit-wrapper ohHide">
                            <div class="row ">
                                <div class="wpv-grid grid-1-1  wpv-first-level first unextended no-extended-padding" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-abf13eb5fe48042dd3f2fa66ea19b4f0">
                                    <h2 style="text-align: center;">ACCORDION STYLE</h2>
                                </div>
                            </div>
                        </div>
                        <div class="limit-wrapper ohHide">
                            <div class="row ">
                                <div class="wpv-grid grid-1-1  wpv-first-level first unextended animation-from-bottom animated-active no-extended-padding" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-0d97e356c17940cb4b08607f4b4f45d2">
                                    <div class="wpv-accordion-wrapper wpv-accordion-1">
                                        <ul class="wpv-accordion" data-collapsible="false">
                                            <li class="pane-wrapper" style="">
                                                <div class="tab">
                                                    <h4 class="inner">HOW WE CAN HELP YOU?</h4>
                                                </div>
                                                <div class="pane">
                                                    <div class="inner">
                                                        <div class="row ">
                                                            <div class="wpv-grid grid-1-1  first unextended no-extended-padding" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-547b796b1dccb31202aec517be37f9e7">
                                                                <p>
                                                                    This is Photoshop’s version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.<br />
                                                                    Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit.<br />
                                                                    Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit.
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="pane-wrapper" style="">
                                                <h4 class="tab">
                                                    <div class="inner">HOW WE CAN HELP YOU?</div>
                                                </h4>
                                                <div class="pane">
                                                    <div class="inner">
                                                        <div class="row ">
                                                            <div class="wpv-grid grid-1-1  first unextended no-extended-padding" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-8f2b771b57a692b6b850c6f65b5473f0">
                                                                <p>
                                                                    This is Photoshop’s version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.<br />
                                                                    Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit.<br />
                                                                    Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit.
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="pane-wrapper" style="">
                                                <h4 class="tab">
                                                    <div class="inner">HOW WE CAN HELP YOU?</div>
                                                </h4>
                                                <div class="pane">
                                                    <div class="inner">
                                                        <div class="row ">
                                                            <div class="wpv-grid grid-1-1  first unextended no-extended-padding" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-34af34b5492093f0862b3d300494aeef">
                                                                <p>
                                                                    This is Photoshop’s version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.<br />
                                                                    Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit.<br />
                                                                    Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit.
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                        <style>
                                            .wpv-accordion-1 .tab .inner {
                                                background-color: #ffffff;
                                                color: #364352;
                                            }
                                        </style>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="push  wpv-hide-lowres" style="height: 50px"></div>
                        <div class="limit-wrapper ohHide">
                            <div class="row ">
                                <div class="wpv-grid grid-1-1  wpv-first-level first unextended animation-from-top animated-active no-extended-padding" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-3bdeec2157ddd2393d44c7397014537d">
                                    <h3 style="text-align: center;">Not found your question? Just ask us!</h3>
                                    <p style="text-align: center;">
                                        Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorperadipiscing elit, sed diam nonummy<br />
                                        nibh  suscipit lobortis nisl ut aliquip ex ea commodo consequat.
                                </div>
                            </div>
                        </div>
                        <div class="push " style="height: 60px"></div>
                        <div class="row  ohHide">
                            <div class="wpv-grid grid-1-1  wpv-first-level parallax-bg first has-background extended has-extended-padding" style="background: url( https_/construction.vamtam.com/wp-content/uploads/2013/03/bg_services.html) no-repeat center top; background-size: cover; background-attachment: fixed; background-color: #F7F7F7; padding-top: 30px; padding-bottom: 30px;"
                                id="wpv-column-732e9dd1118e297292b63f8fa4ff9e7c" data-parallax-method="fixed" data-parallax-inertia="-0.2">
                                <div class="limit-wrapper">
                                    <div class="extended-column-inner">
                                        <style>
                                            #wpv-column-732e9dd1118e297292b63f8fa4ff9e7c p, #wpv-column-732e9dd1118e297292b63f8fa4ff9e7c em, #wpv-column-732e9dd1118e297292b63f8fa4ff9e7c h1, #wpv-column-732e9dd1118e297292b63f8fa4ff9e7c h2, #wpv-column-732e9dd1118e297292b63f8fa4ff9e7c h3, #wpv-column-732e9dd1118e297292b63f8fa4ff9e7c h4, #wpv-column-732e9dd1118e297292b63f8fa4ff9e7c h5, #wpv-column-732e9dd1118e297292b63f8fa4ff9e7c h6, #wpv-column-732e9dd1118e297292b63f8fa4ff9e7c .column-title, #wpv-column-732e9dd1118e297292b63f8fa4ff9e7c .sep-text h2.regular-title-wrapper, #wpv-column-732e9dd1118e297292b63f8fa4ff9e7c .text-divider-double, #wpv-column-732e9dd1118e297292b63f8fa4ff9e7c .sep-text .sep-text-line, #wpv-column-732e9dd1118e297292b63f8fa4ff9e7c .sep, #wpv-column-732e9dd1118e297292b63f8fa4ff9e7c .sep-2, #wpv-column-732e9dd1118e297292b63f8fa4ff9e7c .sep-3, #wpv-column-732e9dd1118e297292b63f8fa4ff9e7c td, #wpv-column-732e9dd1118e297292b63f8fa4ff9e7c th, #wpv-column-732e9dd1118e297292b63f8fa4ff9e7c caption {
                                                color: #787878;
                                            }

                                            #wpv-column-732e9dd1118e297292b63f8fa4ff9e7c:before {
                                                background-color: transparent;
                                            }
                                        </style>
                                        <div class="row ">
                                            <div class="wpv-grid grid-1-1  first unextended no-extended-padding" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-8df3e1f97f8921b5e8e27fa214125438">
                                                <div class="push " style="margin-bottom: -80px"></div>
                                                <p style="text-align: center;">
                                                    <a class="wpv-scroll-to-top" href="#"><span class='icon shortcode theme box use-hover' style='color: #ffffff; font-size: 25px !important;'>&#58905;</span></a><div class="push " style="height: 40px"></div>
                                            </div>
                                        </div>
                                        <div class="row ">
                                            <div class="wpv-grid grid-1-1  first unextended no-extended-padding" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-580600c26d7d4c41b92dc23341a88e07">
                                                <p>
                                                    <div class="wpcf7" id="wpcf7-f9472-p8024-o1">
                                                        <div class="screen-reader-response"></div>
                                                        <div class="wpcf7-form">
                                                            <div style="display: none;">
                                                                <input type="hidden" name="_wpcf7" value="9472" />
                                                                <input type="hidden" name="_wpcf7_version" value="4.9.1" />
                                                                <input type="hidden" name="_wpcf7_locale" value="en_US" />
                                                                <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f9472-p8024-o1" />
                                                                <input type="hidden" name="_wpcf7_container_post" value="8024" />
                                                            </div>
                                                            <div class="vamtam-rsvp-form">
                                                                <div class="row">
                                                                    <div class="grid-1-2">
                                                                        <span class="wpcf7-form-control-wrap your-name">
                                                                            <input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" id="your-name" aria-required="true" aria-invalid="false" placeholder="Name" /></span>
                                                                    </div>
                                                                    <div class="grid-1-2">
                                                                        <span class="wpcf7-form-control-wrap your-email">
                                                                            <input type="text" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" id="Phone" aria-required="true" aria-invalid="false" placeholder="E-mail" /></span>
                                                                    </div>
                                                                    <div class="grid-1-1">
                                                                        <span class="wpcf7-form-control-wrap question">
                                                                            <textarea name="question" cols="40" rows="6" class="wpcf7-form-control wpcf7-textarea wpcf7-validates-as-required" id="message" aria-required="true" aria-invalid="false" placeholder="Your Question is ..."></textarea></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </p>
                                            </div>
                                            <div style="text-align: center;">
                                                <input type="submit" value="Send Your Question →" class="wpcf7-form-control wpcf7-submit" />
                                            </div>
                                        </div>
                                        <div class="wpcf7-response-output wpcf7-display-none"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="push  wpv-hide-lowres" style="height: 40px"></div>
                    </div>
                </article>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="CPScript" runat="server">
</asp:Content>
