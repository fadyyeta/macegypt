﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="Brands.aspx.cs" Inherits="macEgypt.Brands" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CPTitle" runat="server">
    Brands
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPHead" runat="server">
    <link href="CSS/PartnerStyle.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css">



    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700&subset=latin-ext" rel="stylesheet">
    <%--partner review styles--%>
    <style>
        .testimonial-slider {
            /*display: table;*/ /* Allow the centering to work */
            margin: 0 auto;
        }

        blockquote.testimonial {
            text-align: center;
        }

            blockquote.testimonial p {
                color: white;
            }

        #partnersReviewCont ul {
            margin: 0;
        }

            #partnersReviewCont ul li {
                list-style-type: none;
                display: inline;
            }

        #partnersReviewCont {
            background-color: #EA5A4F;
        }

        .owl-carousel.owl-drag .owl-item {
            margin: 50px auto;
        }
    </style>
    <%--other available brands--%>
    <style>
        .content-section-5c5cbd70cb6dd {
            background-color: #fff051;
        }
        .content-section-5c5cbd70bff19 {
            background-color: #f5f5f5;
            padding-top: 25px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CPContent" runat="server">
    <div id="main-content">
        <div id="main" role="main" class="wpv-main layout-full" style="">
            <section class="main-section page-section no-margin">
                <div class="row page-wrapper">

                    <article id="post-2016" class="post-2016 page type-page status-publish hentry text-center">
                        <div class="text">
                            <section class="content-section full-width with-col cover custom-padding no-margin content-section-5c5cbd70bff19" style="margin-left: -108px; padding-left: 108px; margin-right: -108px; padding-right: 108px;">
                                <div class="row vc_row-fluid">
                                    <div class="wpb_column col-md-2"></div>
                                    <div class="wpb_column col-md-8">
                                        <h2 class="section-title mb-70 mb-sm-40 font-alt align-center" style="color: #1c2e2e; margin-top: 10px; margin-bottom: 20px;">BRANDS AND PARTNERS</h2>
                                        <div class="section-text text-block align-center">
                                            <p>We are proud to be official distribution partners to a number of world- renowned brand manufacturers. By having close relationships with these brands, it allows us to offer customers not only high quality products but also direct supply at greater value.</p>
                                        </div>
                                    </div>
                                    <div class="wpb_column col-md-2"></div>
                                </div>
                            </section>
                            <section class="content-section full-width with-col cover custom-padding no-margin content-section-5c5cbd70c0418" style="/*margin-left: -108px; padding-left: 108px; margin-right: -108px; padding-right: 108px;*/ margin:auto;">
                                <div class="container">
                                    <div class="wpb_column col-md-4 col-border wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                                        <div class="full-block align-center">
                                            <a href="http://www.hellottec.com" title="ttec" target="_blank">
                                                <img src="https://www.genuinesolutions.co.uk/wp-content/uploads/2018/11/ttec-2.jpg" width="300px" alt="" title=""></a>
                                        </div>
                                    </div>
                                    <div class="wpb_column col-md-4 col-border wow fadeInRight animated" style="visibility: visible; animation-name: fadeInRight;">
                                        <div class="full-block align-center">
                                            <a href="http://www.jabra.co.uk/" title="Jabra" target="_blank">
                                                <img src="https://www.genuinesolutions.co.uk/wp-content/uploads/2015/03/jabra-logo-1.jpg" width="300px" alt="" title=""></a>
                                        </div>
                                    </div>
                                    <div class="wpb_column col-md-4 col-border wow fadeInLeft animated" style="visibility: visible; animation-name: fadeInLeft;">
                                        <div class="full-block align-center">
                                            <img src="https://www.genuinesolutions.co.uk/wp-content/uploads/2018/01/lamborghini-logo.jpg" width="300px" alt="" title="">
                                        </div>
                                    </div>
                                    <div class="wpb_column col-md-4 col-border wow fadeInLeft animated" style="visibility: visible; animation-name: fadeInLeft;">
                                        <div class="full-block align-center">
                                            <a href="http://www.noisehush.com/" title="Noisehush " target="_blank">
                                                <img src="https://www.genuinesolutions.co.uk/wp-content/uploads/2015/03/noisehush3.jpg" width="300px" alt="" title=""></a>
                                        </div>
                                    </div>
                                    <div class="wpb_column col-md-4 col-border wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                                        <div class="full-block align-center">
                                            <a href="https://www.doro.co.uk/" title="Doro" target="_blank">
                                                <img src="https://www.genuinesolutions.co.uk/wp-content/uploads/2015/03/doro.jpg" width="300px" alt="" title=""></a>
                                        </div>
                                    </div>
                                    <div class="wpb_column col-md-4 col-border wow fadeInRight animated" style="visibility: visible; animation-name: fadeInRight;">
                                        <div class="full-block align-center">
                                            <a href="https://www.duracell.co.uk/">
                                                <img src="https://www.genuinesolutions.co.uk/wp-content/uploads/2015/03/duracell.png" width="300px" alt="" title=""></a>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section class="content-section full-width with-col cover no-padding no-margin content-section-5c5cbd70c605c" style="margin-left: -108px; padding-left: 108px; margin-right: -108px; padding-right: 108px;">
                                <div class="container">
                                    <div class="wpb_column col-md-4 col-border wow fadeInLeft animated" style="visibility: visible; animation-name: fadeInLeft;">
                                        <div class="full-block align-center">
                                            <a href="http://www.kensington.com" title="Kensington" target="_blank">
                                                <img src="https://www.genuinesolutions.co.uk/wp-content/uploads/2015/03/kensington1.jpg" width="300px" alt="" title=""></a>
                                        </div>
                                    </div>
                                    <div class="wpb_column col-md-4 col-border wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                                        <div class="full-block align-center">
                                            <a href="http://uk.jamaudio.com/" title="Jam Audio" target="_blank">
                                                <img src="https://www.genuinesolutions.co.uk/wp-content/uploads/2015/03/jam5.jpg" width="300px" alt="" title=""></a>
                                        </div>
                                    </div>
                                    <div class="wpb_column col-md-4 col-border wow fadeInRight animated" style="visibility: visible; animation-name: fadeInRight;">
                                        <div class="full-block align-center">
                                            <a href="http://intoro.co.uk/" title="Intoro" target="_blank">
                                                <img src="https://www.genuinesolutions.co.uk/wp-content/uploads/2015/03/intoro1.jpg" width="300px" alt="" title=""></a>
                                        </div>
                                    </div>
                                    <div class="wpb_column col-md-4 col-border wow fadeInLeft animated" style="visibility: visible; animation-name: fadeInLeft;">
                                        <div class="full-block align-center">
                                            <a href="https://venomcomms.com/" title="Venom" target="_blank">
                                                <img src="https://www.genuinesolutions.co.uk/wp-content/uploads/2015/03/venom1.jpg" width="300px" alt="" title=""></a>
                                        </div>
                                    </div>
                                    <div class="wpb_column col-md-4 col-border wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                                        <div class="full-block align-center">
                                            <a href="http://www.integralmemory.com/" title="Integral Memory" target="_blank">
                                                <img src="https://www.genuinesolutions.co.uk/wp-content/uploads/2015/03/integral.jpg" width="300px" alt="" title=""></a>
                                        </div>
                                    </div>
                                    <div class="wpb_column col-md-4 col-border wow fadeInRight animated" style="visibility: visible; animation-name: fadeInRight;">
                                        <div class="full-block align-center">
                                            <a href="http://www.alcatelonetouch.com/uk/" title="Alcatel One Touch" target="_blank">
                                                <img src="https://www.genuinesolutions.co.uk/wp-content/uploads/2015/03/alcatel4.jpg" width="300px" alt="" title=""></a>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section class="content-section full-width is-fluid cover custom-padding custom-margin content-section-5c5cbd70c8fbc" style="margin-left: -92.8px; margin-right: -92.8px;">
                                <div id="partnersReviewCont" style="height: 380px; overflow: -webkit-paged-y;">
                                    <div class="limit-wrapper">
                                        <div class="row ">
                                            <section class="yorum">
                                                <div class="container">
                                                    <div class="col-12">
                                                        <div class="owl-carousel partnerRev">
                                                            <div class="">
                                                                <div class="container relative">
                                                                    <div class="row">
                                                                        <div class="col-md-8 col-md-offset-2 align-center">
                                                                            <div class="section-icon  padding-top -20px" style="color: #fff051;">
                                                                                <span class="icon-quote"></span>
                                                                            </div>
                                                                            <h3 class="small-title font-alt text-center" style="color: #fff051; font-size: 24px;">First What our partners are saying</h3>
                                                                            <blockquote class="testimonial" style="color: #1c2e2e;">
                                                                                <p>&#8220;Verbatim&#8217;s relationship with Genuine Solutions began in later 2013, and has proven to be an overwhelmingly positive experience. We are proud of our relationship with Genuine Solutions and hope they will continue their excellent representation of our brand for many years to come.&#8221;</p>
                                                                                <footer class="testimonial-author" style="color: #fff051;">Verbatim</footer>
                                                                            </blockquote>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="">
                                                                <div class="container relative">
                                                                    <div class="row">
                                                                        <div class="col-md-8 col-md-offset-2 align-center">
                                                                            <div class="section-icon  padding-top -20px" style="color: #fff051;">
                                                                                <span class="icon-quote"></span>
                                                                            </div>
                                                                            <h3 class="small-title font-alt text-center" style="color: #fff051; font-size: 24px;">Second What our partners are saying</h3>
                                                                            <blockquote class="testimonial" style="color: #1c2e2e;">
                                                                                <p>&#8220;We have been working in partnership with Genuine Solutions for four years. In that time, working collaboratively, we have seen effective category growth and a real strive for continuous account development. Genuine Solutions are professional, ambitious and fun to work alongside.&#8221;</p>
                                                                                <footer class="testimonial-author" style="color: #fff051;">Integral</footer>
                                                                            </blockquote>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="">
                                                                <div class="container relative">
                                                                    <div class="row">
                                                                        <div class="col-md-8 col-md-offset-2 align-center">
                                                                            <div class="section-icon  padding-top -20px" style="color: #fff051;">
                                                                                <span class="icon-quote"></span>
                                                                            </div>
                                                                            <h3 class="small-title font-alt text-center" style="color: #fff051; font-size: 24px;">Third What our partners are saying</h3>
                                                                            <blockquote class="testimonial" style="color: #1c2e2e;">
                                                                                <p>“It’s been a pleasure to work Genuine Solutions over the last year and our relationship has grown stronger over this time. They are professional in their approach to market and understand customer needs. We look forward to continuing success in the future.”</p>
                                                                                <footer class="testimonial-author" style="color: #fff051;">House of Marley</footer>
                                                                            </blockquote>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="">
                                                                <div class="container relative">
                                                                    <div class="row">
                                                                        <div class="col-md-8 col-md-offset-2 align-center">
                                                                            <div class="section-icon  padding-top -20px" style="color: #fff051;">
                                                                                <span class="icon-quote"></span>
                                                                            </div>
                                                                            <h3 class="small-title font-alt text-center" style="color: #fff051; font-size: 24px;">Fourth What our partners are saying</h3>
                                                                            <blockquote class="testimonial" style="color: #1c2e2e;">
                                                                                <p>“We’re very excited to have launched our relationship with Genuine Solutions as our distribution partner. They have quickly proven they are a highly professional outfit with a great team who we are sure will help consolidate LG Mobile’s proposition to the market.”</p>
                                                                                <footer class="testimonial-author" style="color: #fff051;">LG</footer>
                                                                            </blockquote>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section class="content-section full-width is-fluid cover custom-padding no-margin content-section-5c5cbd70cb6dd row" style="/*margin-left: 10%; margin-right: 10%*/">
                                <div class="row vc_row-fluid">
                                    <div class="wpb_column col-md-12">
                                        <h2 class="section-title mb-70 mb-sm-40 font-alt align-center" style="color: #1c2e2e;">Other available brands</h2>
                                        <div class="small-item-carousel black owl-carousel mb-0 animate-init owl-theme" data-anim-type="fade-in-right-large" data-anim-delay="100" style="opacity: 1; display: block;">
                                            <div class="owl-wrapper-outer">
                                                <div class="owl-wrapper" style="width: 5076px; left: 0px; display: block; transition: all 800ms ease 0s; transform: translate3d(-282px, 0px, 0px);">
                                                    <div class="owl-item" style="width: 282px;">
                                                        <div class="logo-item">
                                                            <img src="https://www.genuinesolutions.co.uk/wp-content/uploads/2015/03/supertooth.png" width="67" height="67" alt="">
                                                        </div>
                                                    </div>
                                                    <div class="owl-item" style="width: 282px;">
                                                        <div class="logo-item">
                                                            <img src="https://www.genuinesolutions.co.uk/wp-content/uploads/2015/03/sony.png" width="67" height="67" alt="">
                                                        </div>
                                                    </div>
                                                    <div class="owl-item" style="width: 282px;">
                                                        <div class="logo-item">
                                                            <img src="https://www.genuinesolutions.co.uk/wp-content/uploads/2015/03/samsung.png" width="67" height="67" alt="">
                                                        </div>
                                                    </div>
                                                    <div class="owl-item" style="width: 282px;">
                                                        <div class="logo-item">
                                                            <img src="https://www.genuinesolutions.co.uk/wp-content/uploads/2015/03/microsoft.png" width="67" height="67" alt="">
                                                        </div>
                                                    </div>
                                                    <div class="owl-item" style="width: 282px;">
                                                        <div class="logo-item">
                                                            <img src="https://www.genuinesolutions.co.uk/wp-content/uploads/2015/03/huawei.png" width="67" height="67" alt="">
                                                        </div>
                                                    </div>
                                                    <div class="owl-item" style="width: 282px;">
                                                        <div class="logo-item">
                                                            <img src="https://www.genuinesolutions.co.uk/wp-content/uploads/2015/03/blackberry.png" width="67" height="67" alt="">
                                                        </div>
                                                    </div>
                                                    <div class="owl-item" style="width: 282px;">
                                                        <div class="logo-item">
                                                            <img src="https://www.genuinesolutions.co.uk/wp-content/uploads/2015/03/blekin.png" width="67" height="67" alt="">
                                                        </div>
                                                    </div>
                                                    <div class="owl-item" style="width: 282px;">
                                                        <div class="logo-item">
                                                            <img src="https://www.genuinesolutions.co.uk/wp-content/uploads/2015/03/htc.png" width="67" height="67" alt="">
                                                        </div>
                                                    </div>
                                                    <div class="owl-item" style="width: 282px;">
                                                        <div class="logo-item">
                                                            <img src="https://www.genuinesolutions.co.uk/wp-content/uploads/2015/03/beats2.png" width="67" height="67" alt="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section class="content-section full-width with-col cover custom-padding no-margin content-section-5c5cbd70cf370 row" style="margin-left: 10%; padding-left: 10%; margin-right: 10%; padding-right: 10%;">
                                <div class="row vc_row-fluid">
                                    <div class="wpb_column col-md-12">
                                        <h2 class="section-title mb-70 mb-sm-40 font-alt align-center" style="color: #1c2e2e;">Want to talk? Get in touch.</h2>
                                        <div role="form" class="wpcf7" id="wpcf7-f1745-p2016-o1" lang="en-US" dir="ltr">
                                            <div class="screen-reader-response"></div>
                                            <form action="/brands/#wpcf7-f1745-p2016-o1" method="post" class="wpcf7-form" novalidate="novalidate">
                                                <div style="display: none;">
                                                    <input type="hidden" name="_wpcf7" value="1745">
                                                    <input type="hidden" name="_wpcf7_version" value="5.1.1">
                                                    <input type="hidden" name="_wpcf7_locale" value="en_US">
                                                    <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f1745-p2016-o1">
                                                    <input type="hidden" name="_wpcf7_container_post" value="2016">
                                                    <input type="hidden" name="g-recaptcha-response" value="">
                                                </div>
                                                <div class="wpcf7-response-output wpcf7-display-none"></div>
                                                <p class="cf-left-col">
                                                    <span class="wpcf7-form-control-wrap your-name">
                                                        <input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Name"></span><span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Email"></span>
                                                </p>
                                                <p class="cf-right-col">
                                                    <span class="wpcf7-form-control-wrap your-message">
                                                        <textarea name="your-message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false" placeholder="Your Message"></textarea></span>
                                                </p>
                                                <p>
                                                    <input type="submit" value="Send" class="wpcf7-form-control wpcf7-submit"><span class="ajax-loader"></span>
                                                </p>
                                                <input type="hidden" id="ct_checkjs_cf7_3cf166c6b73f030b4f67eeaeba301103" name="ct_checkjs_cf7" value="889563919"><script type="text/javascript">setTimeout(function () { var ct_input_name = 'ct_checkjs_cf7_3cf166c6b73f030b4f67eeaeba301103'; if (document.getElementById(ct_input_name) !== null) { var ct_input_value = document.getElementById(ct_input_name).value; document.getElementById(ct_input_name).value = document.getElementById(ct_input_name).value.replace(ct_input_value, '889563919'); } }, 1000);
                                                </script>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </article>
                    <!-- #post-## -->
                </div>
            </section>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="CPScript" runat="server">
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>

    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
    <script>
        $(document).ready(function () {
            $('.owl-carousel.partnerRev').owlCarousel({
                loop: true,
                margin: 10,
                nav: false,
                autoplay: true,
                autoplayTimeout: 3000,
                autoplayHoverPause: true,
                mouseDrag: true,
                touchDrag: true,
                responsive: {
                    0: {
                        items: 1
                    }
                }
            });
        });
    </script>
</asp:Content>
