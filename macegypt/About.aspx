﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="macEgypt.About" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CPTitle" runat="server">
    About Us
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPHead" runat="server">
    <style>
        body .team-member .share-icons .icon {
            border:none;
            background-color:unset !important;
        }
        body .dropcap1 {
            height:50px;
        }
        .wpv-grid.grid-1-1.first.unextended.no-extended-padding {
            padding-left: 5%;
        
        }
        body .team-member .thumbnail img {
            width:100%;
        }
        body .team-member .thumbnail {
            background:unset ;
            padding:unset;

        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CPContent" runat="server">
    <div id="main-content">
        <div id="main" role="main" class="wpv-main layout-full">
            <div class="row page-wrapper">
                <article id="post-7778" class="full post-7778 page type-page status-publish hentry">
                    <div class="page-content">
                        <div class="push " style="height: 30px"></div>
                        <div class="limit-wrapper">
                            <div class="row ">
                                <div class="wpv-grid grid-1-1  wpv-first-level first unextended animation-from-bottom animated-active no-extended-padding animation-ended" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-c195f4af4f502702d5c44feb82f8f90f">
                                    <h3 style="text-align: center;">We are all constructed out of our self dialogue.</h3>
                                    <p style="text-align: center;">
                                        Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorperadipiscing elit, sed diam nonummy<br>
                                        nibh suscipit lobortis nisl ut aliquip ex ea commodo consequat.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="push " style="height: 30px"></div>
                        <div class="row ">
                            <div class="wpv-grid grid-1-1  wpv-first-level parallax-bg first has-background extended has-extended-padding parallax-loaded" style="padding-top: 100px; padding-bottom: 5px;" id="wpv-column-fbffff552df1594e1eaf178c4a3841c5" data-parallax-method="to-centre" data-parallax-inertia="-0.2">
                                <div class="wpv-parallax-bg-img" style="background-position: 50% calc(50% - -99px); background-image: url(&quot;https://construction.vamtam.com/wp-content/uploads/2014/04/bg_about_stats.jpg&quot;); background-color: rgb(247, 247, 247); background-size: auto; background-attachment: fixed; background-repeat: no-repeat;"></div>
                                <div class="wpv-parallax-bg-content">
                                    <div class="limit-wrapper">
                                        <div class="extended-column-inner">
                                            <style>
                                                #wpv-column-fbffff552df1594e1eaf178c4a3841c5 p, #wpv-column-fbffff552df1594e1eaf178c4a3841c5 em, #wpv-column-fbffff552df1594e1eaf178c4a3841c5 h1, #wpv-column-fbffff552df1594e1eaf178c4a3841c5 h2, #wpv-column-fbffff552df1594e1eaf178c4a3841c5 h3, #wpv-column-fbffff552df1594e1eaf178c4a3841c5 h4, #wpv-column-fbffff552df1594e1eaf178c4a3841c5 h5, #wpv-column-fbffff552df1594e1eaf178c4a3841c5 h6, #wpv-column-fbffff552df1594e1eaf178c4a3841c5 .column-title, #wpv-column-fbffff552df1594e1eaf178c4a3841c5 .sep-text h2.regular-title-wrapper, #wpv-column-fbffff552df1594e1eaf178c4a3841c5 .text-divider-double, #wpv-column-fbffff552df1594e1eaf178c4a3841c5 .sep-text .sep-text-line, #wpv-column-fbffff552df1594e1eaf178c4a3841c5 .sep, #wpv-column-fbffff552df1594e1eaf178c4a3841c5 .sep-2, #wpv-column-fbffff552df1594e1eaf178c4a3841c5 .sep-3, #wpv-column-fbffff552df1594e1eaf178c4a3841c5 td, #wpv-column-fbffff552df1594e1eaf178c4a3841c5 th, #wpv-column-fbffff552df1594e1eaf178c4a3841c5 caption {
                                                    color: #787878;
                                                }

                                                #wpv-column-fbffff552df1594e1eaf178c4a3841c5:before {
                                                    background-color: transparent;
                                                }
                                            </style>
                                            <div class="row ">
                                                <div class="wpv-grid grid-1-5  first unextended no-extended-padding" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-1b3cbdfce8599912a339f66af16de87d">
                                                    <div class="wpv-progress number started" data-number="160" style="color: #364352"><span class="icon shortcode theme  use-hover" style=""></span><span>160</span></div>
                                                    <div class="wpv-progress-content">
                                                        <strong>COUNTRIES</strong>
                                                    </div>
                                                </div>
                                                <div class="wpv-grid grid-1-5  unextended no-extended-padding" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-29009d7856a14fb3d0652cda27eb6472">
                                                    <div class="wpv-progress number started" data-number="250" style="color: #364352"><span class="icon shortcode theme  use-hover" style=""></span><span>250</span>+</div>
                                                    <div class="wpv-progress-content">
                                                        <strong>HAPPY CUSTOMERS</strong>
                                                    </div>
                                                </div>
                                                <div class="wpv-grid grid-1-5  unextended no-extended-padding" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-a656df3575e2215f7b2f37da6331fbe3">
                                                    <div class="wpv-progress pie started" data-percent="96" data-bar-color="#F0542D" data-track-color="#D8D8D8" style="color: #3B6097"><span>96</span>%<canvas height="162" width="162" style="height: 130px; width: 130px;"></canvas>
                                                    </div>
                                                    <div class="wpv-progress-content">
                                                        <strong>HAPPY CUSTOMERS!</strong>
                                                    </div>
                                                </div>
                                                <div class="wpv-grid grid-1-5  unextended no-extended-padding" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-a3a87f60ace7f72632d5ee441e8d39a0">
                                                    <div class="wpv-progress number started" data-number="16" style="color: #364352"><span class="icon shortcode theme  use-hover" style=""></span><span>16</span></div>
                                                    <div class="wpv-progress-content">
                                                        <strong>PRODUCTS</strong>
                                                    </div>
                                                </div>
                                                <div class="wpv-grid grid-1-5  unextended no-extended-padding" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-2f729c33c8420015c22225edb441c36e">
                                                    <div class="wpv-progress number started" data-number="10000" style="color: #364352"><span class="icon shortcode theme  use-hover" style=""></span><span>10000</span>+</div>
                                                    <div class="wpv-progress-content">
                                                        <strong>ORDERS</strong>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="push  wpv-hide-lowres" style="height: 80px"></div>
                                            <div class="row ">
                                                <div class="wpv-grid grid-1-1  first unextended no-extended-padding" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-1e8c3e96fce363c6755881dc8d2b6309">
                                                    <p><%--<a href="/team/">Team</a> &nbsp;/&nbsp; <a href="/clients/">Clients </a>&nbsp;/&nbsp; <a href="/contact-us/">Contact</a>--%></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="limit-wrapper">
                            <div class="row ">
                                <div class="wpv-grid grid-3-4  wpv-first-level first unextended has-extended-padding" style="padding-top: 20px; padding-bottom: 0.05px;" id="wpv-column-a1451d74bdc799caf12d378ee46dd3f5">
                                    <div class="row ">
                                        <div class="wpv-grid grid-1-1  first unextended animation-fade-in animated-active no-extended-padding animation-ended" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-dc19323b2a61d60ab71d8d315359bb0f">
                                            <h3>Overview</h3>
                                            <p>Mauris accumsan eros eget libero posuere vulputate. Etiam elit elit, elementum sed varius at, adipiscing vitae est. Sed nec felis pellentesque, lacinia dui sed, ultricies sapien. Pellentesque orci lectus, consectetur vel posuere posuere, rutrum eu ipsum.</p>
                                            <p>Aliquam eget odio sed ligula iaculis consequat at eget orci. Mauris molestie sit amet metus mattis varius. Donec sit amet ligula eget nisi sodales egestas. Mauris non tempor quam, et lacinia sapien. Aliquam interdum dolor aliquet dolor sollicitudin fermentum.</p>
                                            <p>Gonec congue lorem a molestie bibendum. Etiam nisi ante, consectetur eget placerat a, tempus a neque. Donec ut elit urna. Etiam venenatis eleifend urna eget scelerisque. Aliquam in nunc quis dui sollicitudin ornare ac vitae lectus.</p>
                                            <p>
                                                Mauris non tempor quam, et lacinia sapien. Aliquam interdum dolor aliquet dolor sollicitudin fermentum. Donec congue lorem a molestie bibendum. Etiam nisi ante, consectetur eget placerat a, tempus a neque. Donec ut elit urna. Etiam venenatis eleifend urna eget scelerisque. Aliquam in nunc quis dui sollicitudin ornare ac vitae lectus.<br>
                                            </p>
                                            <div class="push " style="height: 40px"></div>
                                        </div>
                                    </div>
                                    <div class="row ">
                                        <div class="wpv-grid grid-1-2  first unextended animation-from-bottom animated-active no-extended-padding animation-ended" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-67a72c01f9d7068d30735ca3afd15dd3">
                                            <div class="clearfix dropcap-wrapper">
                                                <div class="dropcap-left"><span class="dropcap1 ">1987</span></div>
                                                <div class="dropcap-text">Start with a small service</div>
                                            </div>
                                            <div class="push " style="height: 20px"></div>
                                            Sed nec felis pellentesque, lacinia dui sed, ultricies sapien. Pellentesque orci lectus, consectetur vel posuere posuere, rutrum eu ipsum. Aliquam eget odio sed ligula iaculis consequat at eget orci.<p></p>
                                            <p>Mauris molestie sit amet metus mattis varius. Donec sit amet ligula eget nisi sodales egestas.</p>
                                        </div>
                                        <div class="wpv-grid grid-1-2  unextended animation-from-bottom animated-active no-extended-padding animation-ended" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-962599e993d306ef532ef9b74da08d79">
                                            <div class="clearfix dropcap-wrapper">
                                                <div class="dropcap-left"><span class="dropcap1 ">2014</span></div>
                                                <div class="dropcap-text">Service of the year!</div>
                                            </div>
                                            <div class="push " style="height: 20px"></div>
                                            Sed nec felis pellentesque, lacinia dui sed, ultricies sapien. Pellentesque orci lectus, consectetur vel posuere posuere, rutrum eu ipsum. Aliquam eget odio sed ligula iaculis consequat at eget orci.<p></p>
                                            <p>Mauris molestie sit amet metus mattis varius. Donec sit amet ligula eget nisi sodales egestas.</p>
                                        </div>
                                    </div>
                                    <div class="push  wpv-hide-lowres" style="height: 40px"></div>
                                    <div class="row ">
                                        <div class="wpv-grid grid-1-2  first unextended animation-from-bottom animated-active no-extended-padding animation-ended" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-3a046c9e30dd8cb939b8b540413a4a0b">
                                            <div class="clearfix dropcap-wrapper">
                                                <div class="dropcap-left"><span class="dropcap1 ">2014</span></div>
                                                <div class="dropcap-text">Service of the year!</div>
                                            </div>
                                            <div class="push " style="height: 20px"></div>
                                            Sed nec felis pellentesque, lacinia dui sed, ultricies sapien. Pellentesque orci lectus, consectetur vel posuere posuere, rutrum eu ipsum. Aliquam eget odio sed ligula iaculis consequat at eget orci.<p></p>
                                            <p>Mauris molestie sit amet metus mattis varius. Donec sit amet ligula eget nisi sodales egestas.</p>
                                        </div>
                                        <div class="wpv-grid grid-1-2  unextended animation-from-bottom animated-active no-extended-padding animation-ended" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-a6f09bac098ddde84aa23ecb4328729a">
                                            <div class="clearfix dropcap-wrapper">
                                                <div class="dropcap-left"><span class="dropcap1 ">2014</span></div>
                                                <div class="dropcap-text">Service of the year!</div>
                                            </div>
                                            <div class="push " style="height: 20px"></div>
                                            Sed nec felis pellentesque, lacinia dui sed, ultricies sapien. Pellentesque orci lectus, consectetur vel posuere posuere, rutrum eu ipsum. Aliquam eget odio sed ligula iaculis consequat at eget orci.<p></p>
                                            <p>Mauris molestie sit amet metus mattis varius. Donec sit amet ligula eget nisi sodales egestas.</p>
                                        </div>
                                    </div>
                                    <div class="push " style="height: 30px"></div>
                                    <div class="row ">
                                        <div class="wpv-grid grid-5-6  first unextended animation-fade-in animated-active no-extended-padding animation-ended" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-7101c44a03fd9dd1f9590b0eb13f08e7">
                                            <p>
                                                <img class="alignnone size-full wp-image-9884" src="https://construction.vamtam.com/wp-content/uploads/2015/01/sign.png" alt="sign" width="116" height="81"></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="wpv-grid grid-1-4  wpv-first-level unextended has-extended-padding" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-ee76407d03d4841f8189d2d78439471a">
                                    <div class="push  wpv-hide-lowres" style="margin-bottom: -90px"></div>
                                    <div class="row ohHide">
                                        <div class="wpv-grid grid-1-1 add-border first has-background unextended has-extended-padding" style="background: url( 'https://construction.vamtam.com/wp-content/uploads/2014/04/about_right_top.jpg' ) no-repeat center top; background-size: auto; background-attachment: scroll; padding-top: 220px; padding-bottom: 20px;"
                                            id="wpv-column-787ae8c5566fac520ca7ccced4790b57">
                                            <div class="row ">
                                                <div class="wpv-grid grid-1-1  first unextended no-extended-padding" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-f05be246355427f99b7f46a0da14036e">
                                                    <p style="padding-left: 10px;">
                                                        <span class="icon shortcode theme  use-hover" style="color: #F0542D; font-size: 32px !important;"></span>
                                                    </p>
                                                    <h4 style="padding-left: 10px;">24hr Hotline</h4>
                                                    <h1 style="padding-left: 10px;"><span class="accent-1">715.387.5006</span></h1>
                                                    <p style="padding-left: 10px;">Neutra vinyl American Apparel kale chips tofu art party, cardigan quinoa!</p>
                                                    <p style="padding-left: 10px;">
                                                        Tel: 02 562-958<br>
                                                        Mobile: 02 562-95
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="push " style="height: 30px"></div>
                                    <div class="row ohHide">
                                        <div class="wpv-grid grid-1-1 add-border first unextended animation-fade-in animated-active has-extended-padding animation-ended" style="padding-top: 30px; padding-bottom: 20px;" id="wpv-column-61cc4894d1c7d21d552ea8fd5bb5bd46">
                                            <div class="row ">
                                                <div class="wpv-grid grid-1-1  first unextended no-extended-padding" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-cb3db98342a6f3aac952bfaf4130dcf4"><span class="icon shortcode theme  use-hover" style="color: #F0542D; font-size: 32px !important;"></span></div>
                                            </div>
                                            <div class="row ">
                                                <div class="wpv-grid grid-1-1  first unextended no-extended-padding" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-73889ba5db1bb257bbc77d477a4a833d">
                                                    <div class="bxslider-wrapper blockquote-slider">
                                                        <div class="bx-wrapper" style="max-width: 100%;">
                                                            <div class="bx-viewport" style="width: 100%; overflow: hidden; position: relative; height: 393px;">
                                                                <ul class="bxslider-container" id="556f5e4722181fe76ca0b970ebe9b1c8" style="width: 2215%; position: absolute; transition-duration: 0s; transform: translate3d(-261px, 0px, 0px);">
                                                                    <li style="float: left; list-style: none; position: relative; width: 261px;" class="bx-clone">
                                                                        <blockquote class="clearfix small simple post-9665 testimonials type-testimonials status-publish hentry testimonials_category-slider-no-photo">
                                                                            <h3 class="quote-summary">"Create with the heart; build with the mind."</h3>
                                                                            <div class="quote-title-wrapper clearfix">
                                                                                <div class="quote-title"><span class="icon shortcode   use-hover" style="color: #F8DF04;"></span><span class="icon shortcode   use-hover" style="color: #F8DF04;"></span><span class="icon shortcode   use-hover" style="color: #F8DF04;"></span><span class="icon shortcode   use-hover" style="color: #F8DF04;"></span><span class="icon shortcode   use-hover" style="color: #F8DF04;"></span> — <span class="the-title">CRISS JAMI</span></div>
                                                                            </div>
                                                                            <div class="quote-text">
                                                                                <div class="quote-content">
                                                                                    <p>Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. </p>
                                                                                    <p>Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.</p>
                                                                                </div>
                                                                            </div>
                                                                        </blockquote>
                                                                    </li>
                                                                    <li style="float: left; list-style: none; position: relative; width: 261px;">
                                                                        <blockquote class="clearfix small simple post-7587 testimonials type-testimonials status-publish hentry testimonials_category-slider-no-photo">
                                                                            <h3 class="quote-summary">"We are all constructed out of our self dialogue."</h3>
                                                                            <div class="quote-title-wrapper clearfix">
                                                                                <div class="quote-title"><span class="icon shortcode   use-hover" style="color: #F8DF04;"></span><span class="icon shortcode   use-hover" style="color: #F8DF04;"></span><span class="icon shortcode   use-hover" style="color: #F8DF04;"></span><span class="icon shortcode   use-hover" style="color: #F8DF04;"></span><span class="icon shortcode   use-hover" style="color: #F8DF04;"></span> — <span class="the-title">BRYANT MCGILL</span></div>
                                                                            </div>
                                                                            <div class="quote-text">
                                                                                <div class="quote-content">
                                                                                    <p>Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. </p>
                                                                                    <p>Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.</p>
                                                                                </div>
                                                                            </div>
                                                                        </blockquote>
                                                                    </li>
                                                                    <li style="float: left; list-style: none; position: relative; width: 261px;">
                                                                        <blockquote class="clearfix small simple post-9665 testimonials type-testimonials status-publish hentry testimonials_category-slider-no-photo">
                                                                            <h3 class="quote-summary">"Create with the heart; build with the mind."</h3>
                                                                            <div class="quote-title-wrapper clearfix">
                                                                                <div class="quote-title"><span class="icon shortcode   use-hover" style="color: #F8DF04;"></span><span class="icon shortcode   use-hover" style="color: #F8DF04;"></span><span class="icon shortcode   use-hover" style="color: #F8DF04;"></span><span class="icon shortcode   use-hover" style="color: #F8DF04;"></span><span class="icon shortcode   use-hover" style="color: #F8DF04;"></span> — <span class="the-title">CRISS JAMI</span></div>
                                                                            </div>
                                                                            <div class="quote-text">
                                                                                <div class="quote-content">
                                                                                    <p>Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. </p>
                                                                                    <p>Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.</p>
                                                                                </div>
                                                                            </div>
                                                                        </blockquote>
                                                                    </li>
                                                                    <li style="float: left; list-style: none; position: relative; width: 261px;" class="bx-clone">
                                                                        <blockquote class="clearfix small simple post-7587 testimonials type-testimonials status-publish hentry testimonials_category-slider-no-photo">
                                                                            <h3 class="quote-summary">"We are all constructed out of our self dialogue."</h3>
                                                                            <div class="quote-title-wrapper clearfix">
                                                                                <div class="quote-title"><span class="icon shortcode   use-hover" style="color: #F8DF04;"></span><span class="icon shortcode   use-hover" style="color: #F8DF04;"></span><span class="icon shortcode   use-hover" style="color: #F8DF04;"></span><span class="icon shortcode   use-hover" style="color: #F8DF04;"></span><span class="icon shortcode   use-hover" style="color: #F8DF04;"></span> — <span class="the-title">BRYANT MCGILL</span></div>
                                                                            </div>
                                                                            <div class="quote-text">
                                                                                <div class="quote-content">
                                                                                    <p>Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. </p>
                                                                                    <p>Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.</p>
                                                                                </div>
                                                                            </div>
                                                                        </blockquote>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            <div class="bx-controls bx-has-pager">
                                                                <div class="bx-pager bx-default-pager">
                                                                    <div class="bx-pager-item"><a href="" data-slide-index="0" class="bx-pager-link active">1</a></div>
                                                                    <div class="bx-pager-item"><a href="" data-slide-index="1" class="bx-pager-link">2</a></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <script>
                                                            jQuery(function ($) {
                                                                var el = $('#556f5e4722181fe76ca0b970ebe9b1c8');
                                                                el.data('bxslider', el.bxSlider({
                                                                    pager: true,
                                                                    controls: false,
                                                                    auto: true,
                                                                    pause: 7000,
                                                                    autoHover: true,
                                                                    adaptiveHeight: false
                                                                }));
                                                            });
                                                        </script>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="push " style="height: 50px"></div>
                        <div class="row ohHide">
                            <div class="wpv-grid grid-1-1  wpv-first-level first has-background extended has-extended-padding" style="background: url( 'https://construction.vamtam.com/wp-content/uploads/2014/04/about_bg_people1.jpg' ) no-repeat center top; background-size: auto; background-attachment: scroll; background-color: #F0542D; padding-top: 40px; padding-bottom: 40px;"
                                id="wpv-column-f63b0124ce7ee5a7766c2bf879c1804a">
                                <div class="limit-wrapper">
                                    <div class="extended-column-inner">
                                        <style>
                                            #wpv-column-f63b0124ce7ee5a7766c2bf879c1804a p, #wpv-column-f63b0124ce7ee5a7766c2bf879c1804a em, #wpv-column-f63b0124ce7ee5a7766c2bf879c1804a h1, #wpv-column-f63b0124ce7ee5a7766c2bf879c1804a h2, #wpv-column-f63b0124ce7ee5a7766c2bf879c1804a h3, #wpv-column-f63b0124ce7ee5a7766c2bf879c1804a h4, #wpv-column-f63b0124ce7ee5a7766c2bf879c1804a h5, #wpv-column-f63b0124ce7ee5a7766c2bf879c1804a h6, #wpv-column-f63b0124ce7ee5a7766c2bf879c1804a .column-title, #wpv-column-f63b0124ce7ee5a7766c2bf879c1804a .sep-text h2.regular-title-wrapper, #wpv-column-f63b0124ce7ee5a7766c2bf879c1804a .text-divider-double, #wpv-column-f63b0124ce7ee5a7766c2bf879c1804a .sep-text .sep-text-line, #wpv-column-f63b0124ce7ee5a7766c2bf879c1804a .sep, #wpv-column-f63b0124ce7ee5a7766c2bf879c1804a .sep-2, #wpv-column-f63b0124ce7ee5a7766c2bf879c1804a .sep-3, #wpv-column-f63b0124ce7ee5a7766c2bf879c1804a td, #wpv-column-f63b0124ce7ee5a7766c2bf879c1804a th, #wpv-column-f63b0124ce7ee5a7766c2bf879c1804a caption {
                                                color: #fff;
                                            }

                                            #wpv-column-f63b0124ce7ee5a7766c2bf879c1804a:before {
                                                background-color: transparent;
                                            }
                                        </style>
                                        <div class="row ">
                                            <div class="wpv-grid grid-1-1  first unextended no-extended-padding" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-9d9000196e8ffc4bb61587d5e557d337">
                                                <p style="text-align: center;">
                                                    <span class="icon shortcode theme  use-hover" style="color: #ffffff; font-size: 40px !important;"></span>
                                                </p>
                                                <h2 style="text-align: center;">ARCHITECTS &amp; CONSTRUCTORS</h2>
                                            </div>
                                        </div>
                                        <div class="push  wpv-hide-lowres" style="height: 20px"></div>
                                        <div class="row ">
                                            <div class="wpv-grid grid-1-4  first unextended no-extended-padding" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-9cc3eea28e7159ac2971c6136f96eb19">
                                                <div class="team-member ">
                                                    <div class="thumbnail">
                                                        <a href="/team/ben-jonson/" title="Ben Jonson">
                                                            <img width="300" height="300" src="https://construction.vamtam.com/wp-content/uploads/2014/04/people_2.jpg" class="attachment-full size-full" alt="" srcset="https://construction.vamtam.com/wp-content/uploads/2014/04/people_2-150x150@2x.jpg 300w, https://construction.vamtam.com/wp-content/uploads/2014/04/people_2-250x250.jpg 250w, https://construction.vamtam.com/wp-content/uploads/2014/04/people_2-150x150.jpg 150w, https://construction.vamtam.com/wp-content/uploads/2014/04/people_2-290x290.jpg 290w, https://construction.vamtam.com/wp-content/uploads/2014/04/people_2-60x60.jpg 60w, https://construction.vamtam.com/wp-content/uploads/2014/04/people_2-43x43.jpg 43w, https://construction.vamtam.com/wp-content/uploads/2014/04/people_2-262x262.jpg 262w, https://construction.vamtam.com/wp-content/uploads/2014/04/people_2-60x60@2x.jpg 120w, https://construction.vamtam.com/wp-content/uploads/2014/04/people_2-43x43@2x.jpg 86w" sizes="(max-width: 300px) 100vw, 300px">
                                                        </a>
                                                        <div class="share-icons clearfix">
                                                            <a href="/team/ben-jonson/" title="Ben Jonson"></a><a href="/" title=""><span class="icon shortcode theme  use-hover" style=""></span></a>
                                                            <a href="/" title=""><span class="icon shortcode   use-hover" style=""></span></a>
                                                            <a href="/" title=""><span class="icon shortcode   use-hover" style=""></span></a>
                                                        </div>

                                                    </div>
                                                    <div class="team-member-info">
                                                        <h3>
                                                            <a href="/team/ben-jonson/" title="Ben Jonson">Ben Jonson </a>
                                                        </h3>
                                                        <h5 class="regular-title-wrapper team-member-position">CEO &amp; ARCHITECT </h5>
                                                        <div class="team-member-phone"><a href="tel:Tel: 800-700-6200" title="Call Ben Jonson">Tel: 800-700-6200</a></div>
                                                        <div><a href="mailto:support@vamtam.com" title="email Ben Jonson">support@vamtam.com</a></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="wpv-grid grid-1-4  unextended no-extended-padding" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-2d940f76a3d3f6a14ac26d16d0dcc1d3">
                                                <div class="team-member ">
                                                    <div class="thumbnail">
                                                        <a href="/team/ashley-fletcher/" title="Ashley Fletcher">
                                                            <img width="300" height="300" src="https://construction.vamtam.com/wp-content/uploads/2014/04/people_1.jpg" class="attachment-full size-full" alt="" srcset="https://construction.vamtam.com/wp-content/uploads/2014/04/people_1-150x150@2x.jpg 300w, https://construction.vamtam.com/wp-content/uploads/2014/04/people_1-250x250.jpg 250w, https://construction.vamtam.com/wp-content/uploads/2014/04/people_1-150x150.jpg 150w, https://construction.vamtam.com/wp-content/uploads/2014/04/people_1-290x290.jpg 290w, https://construction.vamtam.com/wp-content/uploads/2014/04/people_1-60x60.jpg 60w, https://construction.vamtam.com/wp-content/uploads/2014/04/people_1-43x43.jpg 43w, https://construction.vamtam.com/wp-content/uploads/2014/04/people_1-262x262.jpg 262w, https://construction.vamtam.com/wp-content/uploads/2014/04/people_1-60x60@2x.jpg 120w, https://construction.vamtam.com/wp-content/uploads/2014/04/people_1-43x43@2x.jpg 86w" sizes="(max-width: 300px) 100vw, 300px">
                                                        </a>
                                                        <div class="share-icons clearfix">
                                                            <a href="/team/ashley-fletcher/" title="Ashley Fletcher"></a><a href="/" title=""><span class="icon shortcode theme  use-hover" style=""></span></a>
                                                            <a href="/" title=""><span class="icon shortcode   use-hover" style=""></span></a>
                                                            <a href="/" title=""><span class="icon shortcode   use-hover" style=""></span></a>
                                                        </div>

                                                    </div>
                                                    <div class="team-member-info">
                                                        <h3>
                                                            <a href="/team/ashley-fletcher/" title="Ashley Fletcher">Ashley Fletcher </a>
                                                        </h3>
                                                        <h5 class="regular-title-wrapper team-member-position">ARCHITECT </h5>
                                                        <div class="team-member-phone"><a href="tel:Tel: 800-700-6200" title="Call Ashley Fletcher">Tel: 800-700-6200</a></div>
                                                        <div><a href="mailto:support@vamtam.com" title="email Ashley Fletcher">support@vamtam.com</a></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="wpv-grid grid-1-4  unextended no-extended-padding" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-6da9c765ac91deac9672e0b77e19d872">
                                                <div class="team-member ">
                                                    <div class="thumbnail">
                                                        <a href="/team/avie-beaton/" title="Avie Beaton">
                                                            <img width="300" height="300" src="https://construction.vamtam.com/wp-content/uploads/2014/04/people_3.jpg" class="attachment-full size-full" alt="" srcset="https://construction.vamtam.com/wp-content/uploads/2014/04/people_3-150x150@2x.jpg 300w, https://construction.vamtam.com/wp-content/uploads/2014/04/people_3-250x250.jpg 250w, https://construction.vamtam.com/wp-content/uploads/2014/04/people_3-150x150.jpg 150w, https://construction.vamtam.com/wp-content/uploads/2014/04/people_3-290x290.jpg 290w, https://construction.vamtam.com/wp-content/uploads/2014/04/people_3-60x60.jpg 60w, https://construction.vamtam.com/wp-content/uploads/2014/04/people_3-43x43.jpg 43w, https://construction.vamtam.com/wp-content/uploads/2014/04/people_3-262x262.jpg 262w, https://construction.vamtam.com/wp-content/uploads/2014/04/people_3-60x60@2x.jpg 120w, https://construction.vamtam.com/wp-content/uploads/2014/04/people_3-43x43@2x.jpg 86w" sizes="(max-width: 300px) 100vw, 300px">
                                                        </a>
                                                        <div class="share-icons clearfix">
                                                            <a href="/team/avie-beaton/" title="Avie Beaton"></a><a href="/" title=""><span class="icon shortcode theme  use-hover" style=""></span></a>
                                                            <a href="/" title=""><span class="icon shortcode   use-hover" style=""></span></a>
                                                            <a href="/" title=""><span class="icon shortcode   use-hover" style=""></span></a>
                                                        </div>

                                                    </div>
                                                    <div class="team-member-info">
                                                        <h3>
                                                            <a href="/team/avie-beaton/" title="Avie Beaton">Avie Beaton </a>
                                                        </h3>
                                                        <h5 class="regular-title-wrapper team-member-position">FINANCES </h5>
                                                        <div class="team-member-phone"><a href="tel:Tel: 800-700-6200" title="Call Avie Beaton">Tel: 800-700-6200</a></div>
                                                        <div><a href="mailto:support@vamtam.com" title="email Avie Beaton">support@vamtam.com</a></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="wpv-grid grid-1-4  unextended no-extended-padding" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-5e89dc655b9932d2a7f29f60aec98e7a">
                                                <div class="team-member ">
                                                    <div class="thumbnail">
                                                        <a href="/team/rodney-stratton/" title="Rodney Stratton">
                                                            <img width="300" height="300" src="https://construction.vamtam.com/wp-content/uploads/2014/04/people_4.jpg" class="attachment-full size-full" alt="" srcset="https://construction.vamtam.com/wp-content/uploads/2014/04/people_4-150x150@2x.jpg 300w, https://construction.vamtam.com/wp-content/uploads/2014/04/people_4-250x250.jpg 250w, https://construction.vamtam.com/wp-content/uploads/2014/04/people_4-150x150.jpg 150w, https://construction.vamtam.com/wp-content/uploads/2014/04/people_4-290x290.jpg 290w, https://construction.vamtam.com/wp-content/uploads/2014/04/people_4-60x60.jpg 60w, https://construction.vamtam.com/wp-content/uploads/2014/04/people_4-43x43.jpg 43w, https://construction.vamtam.com/wp-content/uploads/2014/04/people_4-262x262.jpg 262w, https://construction.vamtam.com/wp-content/uploads/2014/04/people_4-60x60@2x.jpg 120w, https://construction.vamtam.com/wp-content/uploads/2014/04/people_4-43x43@2x.jpg 86w" sizes="(max-width: 300px) 100vw, 300px">
                                                        </a>
                                                        <div class="share-icons clearfix">
                                                            <a href="/team/rodney-stratton/" title="Rodney Stratton"></a><a href="/" title=""><span class="icon shortcode theme  use-hover" style=""></span></a>
                                                            <a href="/" title=""><span class="icon shortcode   use-hover" style=""></span></a>
                                                            <a href="/" title=""><span class="icon shortcode   use-hover" style=""></span></a>
                                                        </div>

                                                    </div>
                                                    <div class="team-member-info">
                                                        <h3>
                                                            <a href="/team/rodney-stratton/" title="Rodney Stratton">Rodney Stratton </a>
                                                        </h3>
                                                        <h5 class="regular-title-wrapper team-member-position">CONSTRUCTOR </h5>
                                                        <div class="team-member-phone"><a href="tel:Tel: 800-700-6200" title="Call Rodney Stratton">Tel: 800-700-6200</a></div>
                                                        <div><a href="mailto:support@vamtam.com" title="email Rodney Stratton">support@vamtam.com</a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="push  wpv-hide-lowres" style="height: 30px"></div>
                                        <div class="row ">
                                            <div class="wpv-grid grid-1-1  first unextended animation-zoom-in animated-active no-extended-padding animation-ended" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-d5222c2688e0dbcb64f388523aad6459">
                                                <p class="textcenter"><a href="/team/" target="_self" style="font-size: 12px;" class="vamtam-button accent5  button-border hover-accent2 "><span class="btext" data-text="VIEW ALL PEOPLE »">VIEW ALL PEOPLE »</span></a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row ">
                            <div class="wpv-grid grid-1-1  wpv-first-level first has-background extended animation-from-bottom animated-active has-extended-padding animation-ended" style="background-color: #F4F4F4; padding-top: 40px; padding-bottom: 40px;" id="wpv-column-c1f79b3ac21a7ad9fccdd2abde5d4c07">
                                <div class="limit-wrapper">
                                    <div class="extended-column-inner">
                                        <style>
                                            #wpv-column-c1f79b3ac21a7ad9fccdd2abde5d4c07 p, #wpv-column-c1f79b3ac21a7ad9fccdd2abde5d4c07 em, #wpv-column-c1f79b3ac21a7ad9fccdd2abde5d4c07 h1, #wpv-column-c1f79b3ac21a7ad9fccdd2abde5d4c07 h2, #wpv-column-c1f79b3ac21a7ad9fccdd2abde5d4c07 h3, #wpv-column-c1f79b3ac21a7ad9fccdd2abde5d4c07 h4, #wpv-column-c1f79b3ac21a7ad9fccdd2abde5d4c07 h5, #wpv-column-c1f79b3ac21a7ad9fccdd2abde5d4c07 h6, #wpv-column-c1f79b3ac21a7ad9fccdd2abde5d4c07 .column-title, #wpv-column-c1f79b3ac21a7ad9fccdd2abde5d4c07 .sep-text h2.regular-title-wrapper, #wpv-column-c1f79b3ac21a7ad9fccdd2abde5d4c07 .text-divider-double, #wpv-column-c1f79b3ac21a7ad9fccdd2abde5d4c07 .sep-text .sep-text-line, #wpv-column-c1f79b3ac21a7ad9fccdd2abde5d4c07 .sep, #wpv-column-c1f79b3ac21a7ad9fccdd2abde5d4c07 .sep-2, #wpv-column-c1f79b3ac21a7ad9fccdd2abde5d4c07 .sep-3, #wpv-column-c1f79b3ac21a7ad9fccdd2abde5d4c07 td, #wpv-column-c1f79b3ac21a7ad9fccdd2abde5d4c07 th, #wpv-column-c1f79b3ac21a7ad9fccdd2abde5d4c07 caption {
                                                color: #757575;
                                            }

                                            #wpv-column-c1f79b3ac21a7ad9fccdd2abde5d4c07:before {
                                                background-color: transparent;
                                            }
                                        </style>
                                        <div class="row ">
                                            <div class="wpv-grid grid-1-1  first unextended no-extended-padding" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-82dac03e6f4e4f7b6ec779b646f77b03">
                                                <h3 style="text-align: center;">Pampered your brand with this beautiful theme just for $69&nbsp; <a href="#" target="_self" style="font-size: 14px;" class="vamtam-button accent1  button-style-3 hover-accent2 "><span class="icon shortcode theme  use-hover" style="color: #ffffff; font-size: 22px !important;"></span><span class="btext" data-text="BUY ON THEMEFOREST">BUY ON THEMEFOREST</span></a></h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="limit-wrapper">
                    </div>
                </article>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="CPScript" runat="server">
</asp:Content>
