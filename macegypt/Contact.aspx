﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="macEgypt.Contact" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CPTitle" runat="server">
    Contact Us
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPHead" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CPContent" runat="server">
    <div id="main-content">

        <div id="main" role="main" class="wpv-main layout-full">
            <div class="row page-wrapper">
                <div style="width: 100%">
                    <iframe width="100%" height="600" src="https://maps.google.com/maps?width=100%&amp;height=600&amp;hl=en&amp;coord=30.073688060095222, 31.343372941082638&amp;q=Makrem%20Ebeid%20Ext%2C%20Masaken%20Al%20Mohandesin%2C%20Nasr%20City%2C%20Cairo%20Governorate+(Modern%20Arab%20Communications)&amp;ie=UTF8&amp;t=&amp;z=14&amp;iwloc=B&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"><a href="https://www.maps.ie/create-google-map/">Google Maps iframe generator</a></iframe>
                </div>
                <br />
                <article id="post-31" class="full post-31 page type-page status-publish hentry">
                    <div class="page-content">
                        <div class="limit-wrapper">
                            <div class="row ">
                                <div class="wpv-grid grid-1-1  wpv-first-level first unextended has-extended-padding" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-5112f5ac9b2173bc06cc86b32c78278d">
                                    <h2 class="column-title">WE ARE IN CAIRO, EG</h2>
                                    <div class="row ">
                                        <div class="wpv-grid grid-1-3  first unextended animation-fade-in animated-active no-extended-padding animation-ended" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-ec540c07c6a13106a3061e0f277b00e7">
                                            <p style="text-align: center;">
                                                <span class="icon shortcode theme  use-hover" style="color: #F0542D; font-size: 32px !important;"></span>
                                            </p>
                                            <p style="text-align: center;">
                                                Cairo, Egypt<br>
                                                24 Makram Abied Ext.(Beside City Stars – capital 8)<br>
                                                <a href="#">Map&nbsp;→</a>
                                            </p>
                                        </div>
                                        <div class="wpv-grid grid-1-3  unextended animation-fade-in animated-active no-extended-padding animation-ended" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-66d90176d5720766f3ba46f02ba3e6c2">
                                            <p style="text-align: center;">
                                                <span class="icon shortcode theme  use-hover" style="color: #F0542D; font-size: 32px !important;"></span>
                                            </p>
                                            <p style="text-align: center;">
                                                Tel: 20 - 2 - 24147814<br>
                                                Mobile: 20-12-4092553<br>
                                                Fax: 20 - 2 - 24147814
                                            </p>
                                        </div>
                                        <div class="wpv-grid grid-1-3  unextended animation-fade-in animated-active no-extended-padding animation-ended" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-8bd6fb2b7bc3d970b9fa120c4a581967">
                                            <p style="text-align: center;">
                                                <span class="icon shortcode theme  use-hover" style="color: #F0542D; font-size: 32px !important;"></span>
                                            </p>
                                            <p style="text-align: center;">
                                                WEEK DAYS: 05:00 – 22:00<br>
                                                SATURDAY: 08:00 – 18:00<br>
                                                SUNDAY: CLOSED
                                            </p>
                                        </div>
                                    </div>
                                    <div class="push " style="margin-bottom: -40px"></div>
                                    <div class="sep-3"></div>
                                    <div class="row ">
                                        <div class="wpv-grid grid-1-1  first unextended animation-zoom-in animated-active no-extended-padding animation-ended" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-55214847a671dc6d11c4304020f9285f">
                                            <div class="push " style="margin-bottom: -10px"></div>
                                            <p style="text-align: center;"><a href="#"><span class="icon shortcode   use-hover" style="color: #7C8A8D; font-size: 16px !important;"></span></a> &nbsp;&nbsp;&nbsp;<a href="#"><span class="icon shortcode   use-hover" style="color: #7C8A8D; font-size: 16px !important;"></span></a> &nbsp;&nbsp;&nbsp;<a href="#"><span class="icon shortcode   use-hover" style="color: #7C8A8D; font-size: 16px !important;"></span></a> </p>
                                            <div class="push " style="height: 20px"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row ">
                            <div class="wpv-grid grid-1-1  wpv-first-level first has-background extended has-extended-padding" style="background: url( 'https://construction.vamtam.com/wp-content/uploads/2013/03/bg_services.jpg' ) repeat center center; background-size: auto; background-attachment: fixed; background-color: #f5f5f5; padding-top: 60px; padding-bottom: 40px;"
                                id="wpv-column-0f25982880153f8bca4a5604d6e87535">
                                <div class="limit-wrapper">
                                    <div class="extended-column-inner">
                                        <style>
                                            #wpv-column-0f25982880153f8bca4a5604d6e87535 p, #wpv-column-0f25982880153f8bca4a5604d6e87535 em, #wpv-column-0f25982880153f8bca4a5604d6e87535 h1, #wpv-column-0f25982880153f8bca4a5604d6e87535 h2, #wpv-column-0f25982880153f8bca4a5604d6e87535 h3, #wpv-column-0f25982880153f8bca4a5604d6e87535 h4, #wpv-column-0f25982880153f8bca4a5604d6e87535 h5, #wpv-column-0f25982880153f8bca4a5604d6e87535 h6, #wpv-column-0f25982880153f8bca4a5604d6e87535 .column-title, #wpv-column-0f25982880153f8bca4a5604d6e87535 .sep-text h2.regular-title-wrapper, #wpv-column-0f25982880153f8bca4a5604d6e87535 .text-divider-double, #wpv-column-0f25982880153f8bca4a5604d6e87535 .sep-text .sep-text-line, #wpv-column-0f25982880153f8bca4a5604d6e87535 .sep, #wpv-column-0f25982880153f8bca4a5604d6e87535 .sep-2, #wpv-column-0f25982880153f8bca4a5604d6e87535 .sep-3, #wpv-column-0f25982880153f8bca4a5604d6e87535 td, #wpv-column-0f25982880153f8bca4a5604d6e87535 th, #wpv-column-0f25982880153f8bca4a5604d6e87535 caption {
                                                color: #767676;
                                            }

                                            #wpv-column-0f25982880153f8bca4a5604d6e87535:before {
                                                background-color: transparent;
                                            }
                                        </style>
                                        <div class="row ">
                                            <div class="wpv-grid grid-1-1  first unextended animation-from-bottom animated-active no-extended-padding animation-ended" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-2e6bad7e634bcbe8aa3bec80d89cff77">
                                                <p></p>
                                                <div role="form" class="wpcf7" id="wpcf7-f10-p31-o1" lang="en-US" dir="ltr">
                                                    <div class="screen-reader-response"></div>
                                                    <form action="/contact-us/#wpcf7-f10-p31-o1" method="post" class="wpcf7-form" novalidate="novalidate">
                                                        <div style="display: none;">
                                                            <input type="hidden" name="_wpcf7" value="10">
                                                            <input type="hidden" name="_wpcf7_version" value="4.9.1">
                                                            <input type="hidden" name="_wpcf7_locale" value="en_US">
                                                            <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f10-p31-o1">
                                                            <input type="hidden" name="_wpcf7_container_post" value="31">
                                                        </div>
                                                        <div class="row">
                                                            <div class="grid-1-3">
                                                                <span class="wpcf7-form-control-wrap Name">
                                                                    <input type="text" name="Name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Name" id="txtName" runat="server"></span>
                                                            </div>
                                                            <div class="grid-1-3">
                                                                <span class="wpcf7-form-control-wrap E-mail">
                                                                    <input type="text" name="E-mail" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="E-mail" id="txtMail" runat="server"></span>
                                                            </div>
                                                            <div class="grid-1-3">
                                                                <span class="wpcf7-form-control-wrap Phone">
                                                                    <input type="text" name="Phone" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" placeholder="Phone" id="txtPhone" runat="server"></span>
                                                            </div>
                                                            <div class="grid-1-1">
                                                                <span class="wpcf7-form-control-wrap Subject">
                                                                    <input type="text" name="Subject" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Subject" id="txtSubject" runat="server"></span>
                                                            </div>
                                                            <div class="grid-1-1">
                                                                <span class="wpcf7-form-control-wrap Message">
                                                                    <textarea name="Message" cols="40" rows="6" class="wpcf7-form-control wpcf7-textarea wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Your message..." id="txtBody" runat="server"></textarea></span>
                                                            </div>
                                                            <div style="text-align: center;">
                                                                <%--<input type="submit" value="SEND MESSAGE →" class="wpcf7-form-control wpcf7-submit" id="btnSend" onserverclick="btnSend_ServerClick">--%>
                                                                <asp:Button Text="SEND MESSAGE →" CssClass="wpcf7-form-control wpcf7-submit" runat="server" ID="btnSend" OnClick="btnSend_Click" />
                                                                <span class="ajax-loader"></span>

                                                            </div>
                                                        </div>
                                                        <div class="wpcf7-response-output wpcf7-display-none"></div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="limit-wrapper">
                    </div>
                </article>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="CPScript" runat="server">
</asp:Content>
