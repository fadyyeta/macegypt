﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="NewsData.aspx.cs" Inherits="macEgypt.NewsData" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CPTitle" runat="server">
    <asp:Literal Text="News Title" ID="lblNewsTitle" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPHead" runat="server">
    <style>
        body .related-posts img {
            height: 160px !important;
        }

        #main {
            margin: 20px auto !important;
        }

        ol li, ul li {
            list-style-type: none;
        }

        body article {
            padding: 40px !important;
            width: 70% !important;
            display: inline-block !important;
        }

        body .adverts {
            background-color: red !important;
            width: 25% !important;
            min-height: 800px !important;
            display: inline-block !important;
            z-index: 10000;
        }
    </style>
    <style>
        #main-content {
            padding-top: 5%;
        }

        .product-type-simple {
            margin: 0 20px 20px 0 !important;
        }

        body .btext {
            color: #3B6097 !important;
            padding: 8px !important;
            font-size: 13pt !important;
        }

            body .btext:hover {
                background-color: #3B6097 !important;
                color: white !important;
            }

        body .woocommerce ul.products li.product.first {
            clear: none !important;
        }
        /*body .product {
            height:250px;
            width:33%;
            background-size:cover;
        }
        body .product img {
            height:250px;
            width:33%;
            background-size:cover;
            padding:initial;
            margin:initial;
        }*/
        body .search-field {
            width: 80%;
            border-radius: 5px !important;
        }

        body .header-search.icon.wpv-overlay-search-trigger {
            background: none;
            border: none;
            font-size: 20pt;
            vertical-align: middle;
        }

        body .price del {
            display: inline-table !important;
        }

        body .price {
            margin-top: -31px !important;
            padding-left: 1px !important;
            margin-bottom: 20px;
        }

        body .product-thumbnail {
            margin-bottom: unset !important;
        }

            body .product-thumbnail img {
                margin: unset !important;
            }

        .woocommerce ul.products li.product .add_to_cart_button, .woocommerce ul.products li.product .vamtam-button.product_type_simple {
            margin-top: 0 !important;
        }

        body h2.text-divider-double {
            margin: unset;
            margin-bottom: 10px;
        }
    </style>
    <style>
        .meta-top {
            min-height: 50px;
        }

            .meta-top .comment-count a, .meta-top span.post-date {
                font-size: 16pt !important;
                height: 50px;
                padding-top: 10px;
                vertical-align: middle;
            }

            .meta-top .comment-count a {
                padding-top: 18px;
            }

        .comment-count {
            display: inline-block;
            float: right;
        }

        article.single-post-wrapper {
            margin-top: 0 !important;
            padding-top: 0 !important;
        }

        li.comment:last-of-type {
            border-bottom: none !important;
        }

        .avatar.avatar-73.photo {
            max-width: unset;
            height: -webkit-fill-available;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CPContent" runat="server">
    <div id="main" role="main" class="wpv-main layout-right-only">
        <div class="limit-wrapper">
            <div class="row page-wrapper">
                <aside class="left">
                    <section id="woocommerce_product_categories-6" class="widget woocommerce widget_product_categories">
                        <h4 class="widget-title">Product Categories</h4>
                        <ul class="product-categories">
                            <asp:ListView ID="lvCat" ClientIDMode="Static" runat="server">
                                <ItemTemplate>
                                    <li class="cat-item cat-item-441"><a><%# Eval("name") %></a></li>
                                </ItemTemplate>
                            </asp:ListView>
                        </ul>
                    </section>
                    <section id="woocommerce_product_search-4" class="widget woocommerce widget_product_search">
                        <h4 class="widget-title">SEARCH</h4>
                        <form role="search" method="get" class="woocommerce-product-search" action="https://construction.vamtam.com/">
                            <label class="screen-reader-text" for="woocommerce-product-search-field-0">Search for:</label>
                            <input type="search" id="woocommerce-product-search-field-0" class="search-field" placeholder="Search products&hellip;" value="" name="s" />
                            <button class="header-search icon wpv-overlay-search-trigger">&#57645;</button>
                            <input type="hidden" name="post_type" value="product" />
                        </form>
                    </section>
                    <section id="woocommerce_product_tag_cloud-5" class="widget woocommerce widget_product_tag_cloud">
                        <h4 class="widget-title">TAGS</h4>
                        <div class="tagcloud">
                            <asp:ListView ID="lvTags" ClientIDMode="Static" runat="server">
                                <ItemTemplate>
                                    <a href="../product-tag/brake-pads/index.html" class="tag-cloud-link tag-link-400 tag-link-position-1" style="font-size: <%# Eval("tagSize") %>pt;" aria-label="<%# Eval("name") %> (6 products)"><%# Eval("name") %></a>
                                </ItemTemplate>
                            </asp:ListView>
                        </div>
                    </section>
                </aside>

                <article class="single-post-wrapper right-only post-304 post type-post status-publish format-image has-post-thumbnail hentry category-car-tuning post_format-post-format-image">
                    <div class="page-content loop-wrapper clearfix full">
                        <asp:ListView ID="lvArticle" ClientIDMode="Static" runat="server">
                            <ItemTemplate>
                                <div class="post-article has-image-wrapper single">
                                    <div class="image-post-format clearfix as-normal ">
                                        <div class="post-content-outer single-post">
                                            <div class="meta-top clearfix">
                                                <span class="post-date" itemprop="datePublished"><%# Eval("date", "{0:d MMM, yyyy}") %> </span>
                                                <div class="comment-count">
                                                    <a class="icon theme">&#57369;</a><a><%# Eval("county") %>
                                                        <span class="comment-word visuallyhidden">Comments</span></a>
                                                </div>
                                            </div>
                                            <div class="post-media">
                                                <div class='media-inner'>
                                                    <img style="/*height: 400px; */" src="uploads/<%# Eval("urlImage") %>" class="attachment-theme-single-3 size-theme-single-3 wp-post-image img-responsive" alt="<%# Eval("name") %>" />
                                                </div>
                                            </div>
                                            <div class="post-content the-content">
                                                <p><%# Eval("text") %></p>
                                            </div>
                                            <div class="clearfix share-btns">
                                                <div class="sep-3"></div>
                                                <ul class="socialcount" data-url="https://construction.vamtam.com/caring-for-construction/" data-share-text="Caring for construction" data-media="">
                                                    <li class="facebook">
                                                        <a href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fconstruction.vamtam.com%2Fcaring-for-construction%2F" title="Share on Facebook">
                                                            <span class='icon shortcode   use-hover' style=''>&#58155;</span> <span class="count">Like</span>
                                                        </a>
                                                    </li>
                                                    &nbsp;
                                            <li class="twitter">
                                                <a href="https://twitter.com/intent/tweet?text=https%3A%2F%2Fconstruction.vamtam.com%2Fcaring-for-construction%2F" title="Share on Twitter">
                                                    <span class='icon shortcode   use-hover' style=''>&#58159;</span> <span class="count">Tweet</span>
                                                </a>
                                            </li>
                                                    &nbsp;
                                            <li class="googleplus">
                                                <a href="https://plus.google.com/share?url=https%3A%2F%2Fconstruction.vamtam.com%2Fcaring-for-construction%2F" title="Share on Google Plus">
                                                    <span class='icon shortcode   use-hover' style=''>&#58150;</span> <span class="count">+1</span>
                                                </a>
                                            </li>
                                                    &nbsp;
                                            <li class="pinterest">
                                                <a href="../../pinterest.com/pin/create/button/indexaa6c.html?url=https%3A%2F%2Fconstruction.vamtam.com%2Fcaring-for-construction%2F&amp;media=https%3A%2F%2Fconstruction.vamtam.com%2Fwp-content%2Fuploads%2F2014%2F12%2Fblog-4.jpg" title="Share on Pinterest">
                                                    <span class='icon shortcode   use-hover' style=''>&#58216;</span> <span class="count">Pin it</span>
                                                </a>
                                            </li>
                                                    &nbsp;
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:ListView>
                        <div class="clearboth">
                            <div id="comments-list" class="comments">
                                <ol class="commentaty">
                                    <asp:ListView ID="lvComments" ClientIDMode="Static" runat="server">
                                        <ItemTemplate>
                                            <li id="comment-29" class="comment  bypostauthor  clearfix">
                                                <div class="comment-author">
                                                    <img alt='' src='uploads/<%# Eval("imgUrl") %>' class='avatar avatar-73 photo img-circle' height='73' width='73' />
                                                </div>
                                                <div class="comment-content">
                                                    <div class="comment-meta">
                                                        <h3 class="comment-author-link"><%# Eval("guestName") %></h3>
                                                        <h6 title="11:22 pm" class="comment-time"><%# Eval("date", "{0:d MMM, yyyy}") %></h6>
                                                    </div>
                                                    <p><%# Eval("comment") %></p>
                                                </div>
                                            </li>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </ol>
                            </div>
                            <div class="limit-wrapper">
                                <div id="comments" class="comments-wrapper">
                                    <div class="respond-box">
                                        <div class="respond-box-title sep-text centered keep-always">
                                            <div class="sep-text-before">
                                                <div class="sep-text-line"></div>
                                            </div>
                                            <h5 class="content">Write a comment:</h5>
                                            <div class="sep-text-after">
                                                <div class="sep-text-line"></div>
                                            </div>
                                        </div>
                                        <div id="respond" class="comment-respond">
                                            <h3 id="reply-title" class="comment-reply-title"><small><a rel="nofollow" id="cancel-comment-reply-link" href="index.html#respond" style="display: none;">Cancel reply</a></small></h3>
                                            <div id="commentform" class="comment-form">
                                                <div id="commentCont" runat="server" class="comment-form-comment grid-1-1">
                                                    <label for="comment">Message</label>
                                                    <%--<textarea runat="server" id="comment" name="comment" required="required" placeholder="Write us something nice or just a funny joke..." rows="2"></textarea>--%>
                                                    <asp:TextBox TextMode="MultiLine" ID="comment" ClientIDMode="Static" placeholder="Write us something nice or just a funny joke..." Rows="2" runat="server"></asp:TextBox>
                                                </div>
                                                <div id="autherCont" runat="server" class="comment-form-author form-input grid-1-2">
                                                    <label for="author">Name</label>
                                                    <span class="required">*</span><input runat="server" id="author" name="author" type="text" required="required" value="" size="30" placeholder="John Doe" />
                                                </div>
                                                <div id="mailCont" runat="server" class="comment-form-email form-input grid-1-2">
                                                    <label for="email">Email</label>
                                                    <span class="required">*</span><input runat="server" id="email" name="email" type="email" required="required" value="" size="30" placeholder="email@example.com" />
                                                    <p class="comment-notes grid-1-1">Your email address will not be published.</p>
                                                </div>
                                                <p class="form-submit">
                                                    <%--                                                    <input name="submit" type="submit" id="submit" class="submit" value="Post Comment" />--%>
                                                    <asp:Button name="submit" CssClass="submit" ID="btnComment" OnClick="btnComment_Click" runat="server" Text="Post Comment" />
                                                    <input type='hidden' name='comment_post_ID' value='304' id='comment_post_ID' />
                                                    <input type='hidden' name='comment_parent' id='comment_parent' value='0' />
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
                <div class="clearfix"></div>
                <div class="row ">
                    <div class="wpv-grid grid-1-1  first unextended no-extended-padding" style="" id="wpv-column-c60f940e2581185ddcab2637caa8fe0f">
                        <h2 class="text-divider-double">LATEST PRODUCTS</h2>
                        <div class="sep"></div>
                        <div class="woocommerce columns-3 ">
                            <ul class="products">
                                <asp:ListView ID="lvRelated" ClientIDMode="Static" runat="server">
                                    <ItemTemplate>
                                        <li class="post-10025 product type-product status-publish has-post-thumbnail product_cat-garden-garage product_cat-hand-tools product_cat-saws product_tag-green product_tag-tags product_tag-tools first instock sale shipping-taxable purchasable product-type-simple col-sm-3 col-xs-12">
                                            <a href="Product.aspx?ID=<%# Eval("ID") %>">
                                                <span class="onsale">Sale!</span>
                                                <div class="product-thumbnail">
                                                    <img width="290" height="290" src="../wp-content/uploads/2015/01/product-8.1-290x290.jpg" class="attachment-shop_catalog size-shop_catalog wp-post-image" alt="" />
                                                </div>
                                                <span class="price"><del><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&pound;</span>56.00</span></del> <ins><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&pound;</span><%# Eval("price") %></span></ins></span>
                                                <h5><%# Eval("name") %></h5>
                                                <h3 class="woocommerce-loop-product__title"><%# Eval("name") %> &#8211; Category Name</h3>
                                            </a>
                                            <div style="display: none;" class="aggregateRating" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
                                            </div>
                                            <asp:Button runat="server" ID="btnAdd2Cart" CssClass="vamtam-button button-border accent1 hover-accent1  product_type_simple add_to_cart_button ajax_add_to_cart btext" Text="Add to cart"></asp:Button>
                                        </li>
                                    </ItemTemplate>
                                </asp:ListView>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="CPScript" runat="server">
</asp:Content>
