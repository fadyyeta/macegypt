﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="Shop.aspx.cs" Inherits="macEgypt.Shop" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CPTitle" runat="server">
    Products
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPHead" runat="server">
    <style>
        #main-content {
            padding-top: 5%;
        }

        .product-type-simple {
            margin: 0 20px 20px 0 !important;
        }

        body .btext {
            color: #3B6097 !important;
            padding: 8px !important;
            font-size: 13pt !important;
        }

        body .btext:hover {
            background-color: #3B6097 !important;
            color: white !important;
        }

        body .woocommerce ul.products li.product.first {
            clear: none !important;
        }
        /*body .product {
            height:250px;
            width:33%;
            background-size:cover;
        }
        body .product img {
            height:250px;
            width:33%;
            background-size:cover;
            padding:initial;
            margin:initial;
        }*/
        body .search-field {
            width: 80%;
            border-radius: 5px !important;
        }

        body .header-search.icon.wpv-overlay-search-trigger {
            background: none;
            border: none;
            font-size: 20pt;
            vertical-align: middle;
        }

        body .price del {
            display: inline-table !important;
        }

        body .price {
            margin-top: -31px !important;
            padding-left: 1px !important;
            margin-bottom: 20px;
        }

        body .product-thumbnail {
            margin-bottom: unset !important;
        }

        body .product-thumbnail img {
            margin: unset !important;
        }

        .woocommerce ul.products li.product .add_to_cart_button, .woocommerce ul.products li.product .vamtam-button.product_type_simple {
            margin-top: 0 !important;
        }

        body h2.text-divider-double {
            margin: unset;
            margin-bottom: 10px;
        }

        .myAnch {
            cursor: pointer;
        }
        .linkAside {
            cursor: default;
        }

        .myAnch:hover, .linkAside:hover {
            text-shadow: 1px 1px #313439;
        }

        .rightCount {
            float: right;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CPContent" runat="server">
    <div id="main-content">

        <div id="main" role="main" class="wpv-main layout-left-only">
            <div class="limit-wrapper">
                <div class="row page-wrapper">
                    <aside class="left">
                        <section id="woocommerce_product_categories-6" class="widget woocommerce widget_product_categories">
                            <h4 class="widget-title">Lines Of Business</h4>
                            <ul class="product-categories">
                                <asp:ListView ID="lvLOB" ClientIDMode="Static" runat="server">
                                    <ItemTemplate>
                                        <li class="cat-item cat-item-441 linkAside"><a ids="LOB" code="<%# Eval("ID") %>" class="myAnch"><%# Eval("name") %> </a><span class="rightCount">(<%# Eval("countlob") %>)</span> </li>
                                    </ItemTemplate>
                                </asp:ListView>
                            </ul>
                        </section>
                        <section id="woocommerce_product_categories-6" class="widget woocommerce widget_product_categories">
                            <h4 class="widget-title">Product Brands</h4>
                            <ul class="product-categories">
                                <asp:ListView ID="lvBrands" ClientIDMode="Static" runat="server">
                                    <ItemTemplate>
                                        <li class="cat-item cat-item-441 linkAside"><a ids="Brand" code="<%# Eval("ID") %>" class="myAnch"><%# Eval("name") %> </a><span class="rightCount">(<%# Eval("countbrand") %>)</span></li>
                                    </ItemTemplate>
                                </asp:ListView>
                            </ul>
                        </section>
                        <section id="woocommerce_product_categories-6" class="widget woocommerce widget_product_categories">
                            <h4 class="widget-title">Product Categories</h4>
                            <ul class="product-categories">
                                <asp:ListView ID="lvCat" ClientIDMode="Static" runat="server">
                                    <ItemTemplate>
                                        <li class="cat-item cat-item-441 linkAside"><a ids="Category" code="<%# Eval("ID") %>" class="myAnch"><%# Eval("name") %> </a><span class="rightCount">(<%# Eval("countCat") %>)</span></li>
                                    </ItemTemplate>
                                </asp:ListView>
                            </ul>
                        </section>
                        <section id="woocommerce_product_search-4" class="widget woocommerce widget_product_search">
                            <h4 class="widget-title">SEARCH</h4>
                            <form role="search" method="get" class="woocommerce-product-search" action="https://construction.vamtam.com/">
                                <label class="screen-reader-text" for="woocommerce-product-search-field-0">Search for:</label>
                                <input type="search" id="woocommerce-product-search-field-0" class="search-field" placeholder="Search products&hellip;" value="" name="s" />
                                <button class="header-search icon wpv-overlay-search-trigger">&#57645;</button>
                                <input type="hidden" name="post_type" value="product" />
                            </form>
                        </section>
                        <section id="woocommerce_product_tag_cloud-5" class="widget woocommerce widget_product_tag_cloud">
                            <h4 class="widget-title">TAGS</h4>
                            <div class="tagcloud">
                                <asp:ListView ID="lvTags" ClientIDMode="Static" runat="server">
                                    <ItemTemplate>
                                        <a href="../product-tag/brake-pads/index.html" class="tag-cloud-link tag-link-400 tag-link-position-1" style="font-size: <%# Eval("tagSize") %>pt;" aria-label="<%# Eval("name") %> (6 products)"><%# Eval("name") %></a>
                                    </ItemTemplate>
                                </asp:ListView>
                            </div>
                        </section>
                    </aside>
                    <article id="post-9972" class="left-only post-9972 page type-page status-publish hentry">
                        <div class="page-content">
                            <div class="row ">
                                <div class="wpv-grid grid-1-1  wpv-first-level first unextended no-extended-padding" style="" id="wpv-column-22d7fdfa7ed2ca43c1d9e2a2f8c8ff58">
                                    <%--<div class="push  wpv-hide-lowres" style="margin-bottom: -76px"></div>--%>

                                    <div class="row ">
                                        <div class="wpv-grid grid-1-1  first unextended no-extended-padding" style="" id="wpv-column-c60f940e2581185ddcab2637caa8fe0f">
                                            <h2 class="text-divider-double">LATEST PRODUCTS</h2>
                                            <div class="sep"></div>
                                            <div class="woocommerce columns-3 ">
                                                <ul class="products">
                                                    <asp:UpdatePanel ID="upProducts" ClientIDMode="Static" UpdateMode="Conditional" runat="server">
                                                        <ContentTemplate>
                                                            <asp:ListView ID="lvRelated" ClientIDMode="Static" runat="server">
                                                                <ItemTemplate>
                                                                    <li class="post-10025 product type-product status-publish has-post-thumbnail product_cat-garden-garage product_cat-hand-tools product_cat-saws product_tag-green product_tag-tags product_tag-tools first instock sale shipping-taxable purchasable product-type-simple col-sm-5 col-xs-12">
                                                                        <a href="Product.aspx?ID=<%# Eval("ID") %>">
                                                                            <span class="onsale">Sale!</span>
                                                                            <div class="product-thumbnail">
                                                                                <img width="290" height="290" src="../wp-content/uploads/2015/01/product-8.1-290x290.jpg" class="attachment-shop_catalog size-shop_catalog wp-post-image" alt="" />
                                                                            </div>
                                                                            <span class="price"><del><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&pound;</span>56.00</span></del> <ins><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&pound;</span><%# Eval("price") %></span></ins></span>
                                                                            <h5><%# Eval("name") %></h5>
                                                                            <h3 class="woocommerce-loop-product__title"><%# Eval("name") %> &#8211; Category Name</h3>
                                                                        </a>
                                                                        <div style="display: none;" class="aggregateRating" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
                                                                        </div>
                                                                        <asp:Button runat="server" ID="btnAdd2Cart" CssClass="vamtam-button button-border accent1 hover-accent1  product_type_simple add_to_cart_button ajax_add_to_cart btext" Text="Add to cart"></asp:Button>
                                                                        <%--<span class="btext" data-text="Add to cart"></span>--%>
                                                                        
                                                                    </li>
                                                                </ItemTemplate>
                                                            </asp:ListView>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="limit-wrapper">
                        </div>
                    </article>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="CPScript" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {

        });
        <%--var UpdatePanel1 = '<%=upProducts.ClientID%>';

        function ShowItems() {
            if (UpdatePanel1 != null) {
                __doPostBack(UpdatePanel1, '');
            }
        }--%>
        //function to create new querystring before reload ..... used to filter products


        $(".myAnch").click(function () {
            var str = $(location).attr("href");
            var pagy = str.substring(0, str.indexOf('?'));
            str = str.replace(/\,/g, '++');
            var res = str.substring(str.indexOf('?') + 1, str.length);
            var arr = res.split('&');
            var arrQS = [];
            var myItem = $(this);
            var tbl = $(myItem).attr("ids");
            var codeID = $(myItem).attr("code");
            var itemName = $(myItem).text();
            var final = res;
            if (str.indexOf("?") !== -1) {
                if (str.includes(tbl + "=")) {
                    for (var i = 0; i < arr.length; i++) {
                        var tblNm = arr[i].substring(0, arr[i].indexOf('='));
                        var valNm = arr[i].substring(arr[i].indexOf('=') + 1, arr[i].length);

                        if (tblNm == tbl) {
                            valNm += '|||' + itemName;
                        }
                        arrQS.push(tblNm + '=' + valNm);

                    }
                    final = arrQS.toString();
                }
                else {
                    final = res + '&' + tbl + '=' + itemName;
                }
                final = final.replace(/\,/g, '&');
                final = final.replace(/\++/g, ',');
                final = final.replace('|||', ',');
                $(location).attr('href', pagy + '?' + final);
            }
            else {
                $(location).attr('href', pagy + '?' + tbl + '=' + itemName);
            }
        });
    </script>

</asp:Content>
