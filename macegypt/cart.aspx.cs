﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.UI.HtmlControls;

namespace macEgypt
{
    public partial class cart : System.Web.UI.Page
    {
        ClassCode codeClass = new ClassCode();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                loadCart();
            }
        }

        protected void loadCart()
        {
            DataSet ds = codeClass.SQLREAD("select ID,customer,Qty,(select name from Product where Product.ID = cart.productID) as prodName,(select price from Product where Product.ID = cart.productID) as Price , (select imageUrl from Product where Product.ID = cart.productID) as imageUrl, (select(cart.qty * Product.price) from Product where Product.ID = cart.productID) as total from cart where customer = " + Session["userID"]);
            lvOrders.DataSource = ds;
            lvOrders.DataBind();
            if (ds == null || ds.Tables[0] == null || ds.Tables[0].Rows.Count == 0)
            {

                HtmlGenericControl div = new HtmlGenericControl("div");
                div.Attributes["class"] = "option-box jumbotron text-center";
                div.InnerText = "sorry no products are available";

                cartContent.Controls.Add(div);
                tblCart.Visible = false;
            }
        }
    }
}