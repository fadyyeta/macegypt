﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace macEgypt
{
    public partial class App : System.Web.UI.Page
    {
        ClassCode codeClass = new ClassCode();
        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["accType"] == null)
            {
                Session["accType"] = "none";
            }
            string st = Session["accType"].ToString();
            if (st != "Admin")
            {
                Response.Redirect("default.aspx");
            }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            loadAdminPagesList();

            if ((this.ViewState["XMLFILE"] != null))
                Evo1.XMLfile = System.Convert.ToString(this.ViewState["XMLFILE"]);
            if ((this.ViewState["DBAllowInsert"] != null))
                Evo1.DBAllowInsert = System.Convert.ToBoolean(this.ViewState["DBAllowInsert"]);
            if ((this.ViewState["DBAllowUpdate"] != null))
                Evo1.DBAllowUpdate = System.Convert.ToBoolean(this.ViewState["DBAllowUpdate"]);
            if ((this.ViewState["DBAllowDelete"] != null))
                Evo1.DBAllowDelete = System.Convert.ToBoolean(this.ViewState["DBAllowDelete"]);
            if ((this.ViewState["DBAllowSearch"] != null))
                Evo1.DBAllowSearch = System.Convert.ToBoolean(this.ViewState["DBAllowSearch"]);
            if (Request.QueryString["XID"] == "XML")
                Evo1.XMLfile = "XML\\" + "RCMS_XML.XML";
            else
            {
                codeClass.Get_RCMS_XML(Request.QueryString["XID"]);
                Evo1.XMLfile = "XML\\" + Request.QueryString["XID"] + ".XML";
            }

            try
            {
                DataTable dt = new DataTable();
                //dt = codeClass.SQLREAD("SELECT * FROM RCMS_PERM_PAGE WHERE ROLE_ID=" + Session["ROLE_ID"].ToString() +
                //                    " AND PAGE_ID=" + Request.QueryString["PID"].ToString()).Tables[0];
                dt = codeClass.SQLREAD("SELECT * FROM RCMS_PERM_PAGE ").Tables[0];
                if (dt.Rows.Count == 0)
                {
                }

                try
                {
                    Evo1.DBAllowInsert = (bool)dt.Rows[0]["C_Insert"];
                    Evo1.DBAllowUpdate = (bool)dt.Rows[0]["C_Update"];
                    Evo1.DBAllowDelete = (bool)dt.Rows[0]["C_Delete"];
                    Evo1.DBAllowSearch = (bool)dt.Rows[0]["C_Search"];

                }
                catch (Exception ex)
                {
                }
            }
            catch (Exception ex)
            {
            }
        }

        protected void loadAdminPagesList()
        {
            Perm_Pages_Repeater.DataSource = codeClass.SQLREAD("SELECT * FROM RCMS_PAGE").Tables[0];
            Perm_Pages_Repeater.DataBind();
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            this.ViewState.Add("XMLFILE", Evo1.XMLfile);
            this.ViewState.Add("DBAllowInsert", Evo1.DBAllowInsert);
            this.ViewState.Add("DBAllowUpdate", Evo1.DBAllowUpdate);
            this.ViewState.Add("DBAllowDelete", Evo1.DBAllowDelete);
            this.ViewState.Add("DBAllowSearch", Evo1.DBAllowSearch);
        }

        protected void Evo1_DBChange(object sender, Evolutility.UIServer.DatabaseEventArgs e)
        {
            Response.Redirect(HttpContext.Current.Request.Url.AbsoluteUri);
        }

    }
}