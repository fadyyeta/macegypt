﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="App.aspx.cs" Inherits="macEgypt.App" Debug="true" EnableEventValidation="false" EnableViewState="true" ValidateRequest="false" %>

<%@ Register Assembly="Evolutility.UIServer" Namespace="Evolutility" TagPrefix="EVOL" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CPTitle" runat="server">
    <asp:Literal Text="Admin Panel" runat="server" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPHead" runat="server">
    <link href="Evol.css" rel="stylesheet" />
    <script type="text/javascript" src="scripts/jquery-1.3.2.min.js"></script>
    <style>
        ul {
            list-style-type: none;
        }

        .bottom.solid {
            display: none;
        }

        section.container-fluid {
            margin-bottom: 5%;
            background-color: white;
            padding: 20px;
        }

        .fieldImg {
            height: 250px !important;
        }
    </style>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CPContent" runat="server">
    <section class="container-fluid">
        <div class="container">
            <div class="col-md-3">
                <ul class="topnav menu-left-nest">
                    <li>
                        <a href="#" style="border-left: 0px solid !important;" class="title-menu-left">
                            <span class="widget-menu"></span>
                            <i data-toggle="tooltip" class="entypo-cog pull-right config-wrap"></i>
                        </a>
                    </li>
                    <asp:Repeater ID="Perm_Pages_Repeater" runat="server">
                        <ItemTemplate>
                            <li>
                                <a class="tooltip-tip ajax-load" href="App.aspx?XID=<%#Eval("XML_ID")%>&PID=<%#Eval("ID")%>" title="">
                                    <i class="icon-feed"></i>
                                    <span><%#Eval("NAME")%></span>
                                </a>
                            </li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ul>
            </div>
            <div class="col-md-9">
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="nest" id="inlineClose">
                                <div class="title-alt">
                                    <h6></h6>
                                    <div class="titleClose">
                                    </div>
                                    <div class="titleToggle">
                                        <a class="nav-toggle-alt" href="#inline">
                                            <span class="entypo-up-open"></span>
                                        </a>
                                    </div>

                                </div>

                                <div class="body-nest" id="inline">



                                    <EVOL:UIServer ID="Evo1" runat="server" BackColor="#EDEDED" BackColorRowMouseOver="Beige"
                                        CssClass="main1" DBAllowDesign="true" DBAllowExport="True" DisplayModeStart="List" ShowTitle="true"
                                        Language="EN" NavigationLinks="true" RowsPerPage="20"
                                        VirtualPathDesigner="EvoDico/" SecurityKey="EvoDico"
                                        ToolbarPosition="top" VirtualPathPictures="uploads/" VirtualPathToolbar="PixEvo"
                                        Width="100%" DBAllowLogout="False" DBAllowCharts="false" />
                                    <asp:Label ID="Label1" runat="server" Text=""></asp:Label>



                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="CPScript" runat="server">
</asp:Content>
