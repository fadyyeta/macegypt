﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace macEgypt
{
    public partial class Shop : System.Web.UI.Page
    {
        ClassCode codeClass = new ClassCode();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                loadRelated();
                loadAside();
            }
        }
        protected void loadRelated()
        {

            string conditions = "";
            int i = 0;
            string ANDy = "";
            foreach (String key in Request.QueryString.AllKeys)
            {
                string myVal = Request.QueryString[key].ToString();

                if (i == 0)
                {
                    conditions += " WHERE ";
                }
                if (i > 0)
                {
                    ANDy = " AND ";
                }
                if (myVal.Contains(","))
                {
                    string[] vals = myVal.Split(',');
                    myVal = vals[0];
                    for (int x = 1; x < vals.Length; x++)
                    {
                        myVal += "' OR name = N'"+ vals[x];
                    }
                }
                conditions += ANDy+" " +key+"ID in (select ID from "+key+" where name = N'" + myVal + "' ) ";
                i++;
            }

            DataSet ds = codeClass.SQLREAD("select * from Product " + conditions + " order by ID desc");
            lvRelated.DataSource = ds;
            lvRelated.DataBind();


        }
        protected void loadAside()
        {
            DataSet dsCat = codeClass.SQLREAD("select *,(SELECT COUNT(categoryID) FROM Product p  where p.categoryID = Category.ID ) as countCat from category");
            lvCat.DataSource = dsCat;
            lvCat.DataBind();

            DataSet dsTags = codeClass.SQLREAD("select * from Tags");
            lvTags.DataSource = dsTags;
            lvTags.DataBind();

            DataSet dsLOB = codeClass.SQLREAD("select *,(SELECT COUNT(lobID) FROM Product p  where p.lobID = lob.ID ) as countlob from lob");
            lvLOB.DataSource = dsLOB;
            lvLOB.DataBind();

            DataSet dsBrand = codeClass.SQLREAD("select *,(SELECT COUNT(brandID) FROM Product p  where p.brandID = brand.ID ) as countbrand from brand");
            lvBrands.DataSource = dsBrand;
            lvBrands.DataBind();
        }
    }
}