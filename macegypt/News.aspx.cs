﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace macEgypt
{
    public partial class News : System.Web.UI.Page
    {
        ClassCode codeClass = new ClassCode();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                loadNews();
            }

        }
        protected void loadNews()
        {
            DataSet ds = codeClass.SQLREAD("select *, (select count(ID)  from comments where postID = News.ID And postType = N'News') as county from News");
            lvNewssList.DataSource = ds;
            lvNewssList.DataBind();
        }
    }
}