﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="Compatibility.aspx.cs" Inherits="macEgypt.Compatibility" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CPTitle" runat="server">
    Compatible Products
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPHead" runat="server">
    <style>
        body .wwdtext {
            word-wrap: break-word;
            word-break: normal;
        }

        .page-header .title {
            padding: 0 0 40px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CPContent" runat="server">
    <div id="main-content">
        <div id="sub-header" class="layout-full has-background">
            <div class="meta-header" style="">
                <div class="limit-wrapper">
                    <div class="meta-header-inside">
                        <header class="page-header ">
                            <div class="page-header-content">
                                <h1 style="">
                                    <span class="title">
                                        <span itemprop="headline">COMPATIBLE PRODUCTS</span>
                                    </span>
                                </h1>
                            </div>
                        </header>
                    </div>
                </div>
            </div>
        </div>
        <div id="main" role="main" class="wpv-main layout-full">
            <div class="limit-wrapper">
                <div class="row page-wrapper">
                    <article id="post-277" class="full post-277 page type-page status-publish hentry">
                        <div class="page-content">
                            <div class="push  wpv-hide-lowres" style="height: 40px"></div>
                            <div class="row ">
                                <div class="wpv-grid grid-2-3  wpv-first-level first unextended no-extended-padding" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-b6b0a3c50843021a3835ad61f3322943">
                                    <div class="loop-wrapper clearfix news row masonry paginated wpv-isotope-loaded" data-columns="3" style="position: relative; height: 1682.6px;">

                                        <asp:ListView ID="lvNewssList" ClientIDMode="Static" runat="server">
                                            <ItemTemplate>
                                                <div class="page-content post-header grid-2-2 list-item post-304 post type-post status-publish format-image has-post-thumbnail hentry category-car-tuning post_format-post-format-image isotope-item col-md-6 col-sm-6 col-xs-12">
                                                    <div>
                                                        <div class="post-article has-image-wrapper ">
                                                            <div class="image-post-format clearfix as-normal ">
                                                                <div class="post-media-date">
                                                                    <div class="thumbnail" style="height: 248px;">
                                                                        <a href="NewsData.aspx?ID=<%# Eval("ID") %>" title="<%# Eval("name") %>">
                                                                            <img width="360" height="240" src="uploads/<%# Eval("urlImage") %>" class="attachment-theme-normal-3 size-theme-normal-3 wp-post-image img-responsive" alt="<%# Eval("name") %>" style="background-size: cover !important; height: 248px;">
                                                                            <span class="icon shortcode theme  use-hover" style=""></span> </a>
                                                                    </div>
                                                                    <div class="post-actions-wrapper clearfix">
                                                                        <div class="post-date">
                                                                            <%# Eval("date", "{0:d MMM, yyyy}") %>
                                                                        </div>
                                                                        <div class="comment-count">
                                                                            <a href="NewsData.aspx?ID=<%# Eval("ID") %>"><span class="icon"></span><%# Eval("county") %>
                                                                                <span class="comment-word visuallyhidden">Comments</span></a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="post-content-wrapper">
                                                                    <header class="single">
                                                                        <div class="content">
                                                                            <h3>
                                                                                <a href="NewsData.aspx?ID=<%# Eval("ID") %>" title="<%# Eval("name") %>" class="entry-title"><%# Eval("name") %></a>
                                                                            </h3>
                                                                        </div>
                                                                    </header>
                                                                    <div class="post-content-outer">
                                                                        <p class="wwdtext"><%# Eval("text") %></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:ListView>

                                    </div>
                                    <%--<div class="load-more">
                                        <a href="/blog/blog-layout-3/?paged=2" class="lm-btn button clearboth"><span>Load more</span></a>
                                    </div>--%>
                                </div>
                            </div>
                            <div class="push  wpv-hide-lowres" style="height: 50px"></div>
                        </div>
                        <div class="limit-wrapper">
                        </div>
                    </article>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="CPScript" runat="server">
    <script>
        $(document).ready(function () {

            $('.wwdtext').each(function (ind, elem) {
                var newtext = $(elem).text($(elem).text().substring(0, 100)).text();
                newtext += "<span>... Read More</span>";
                $(elem).html(newtext);
            });
        });
    </script>
</asp:Content>

